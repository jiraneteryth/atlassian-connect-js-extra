(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.connectHostCookie = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
/**
* Allows add-ons to store, retrieve and erased cookies against the host JIRA / Confluence. These cannot be seen by other add-ons.
* @exports cookie
*/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});
function prefixCookie(addonKey, name) {
  if (!addonKey || addonKey.length === 0) {
    throw new Error('addon key must be defined on cookies');
  }

  if (!name || name.length === 0) {
    throw new Error('Name must be defined');
  }
  return addonKey + '$$' + name;
}

function addonKeyFromCallback(callback) {
  if (callback && callback._context) {
    return callback._context.extension.addon_key;
  } else {
    throw new Error('addon key not found in callback');
  }
}

exports['default'] = {
  /**
  * Save a cookie.
  * @param name {String} name of cookie
  * @param value {String} value of cookie
  * @param expires {Number} number of days before cookie expires
  * @noDemo
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  */
  save: function save(name, value, expires) {
    var callback = arguments[arguments.length - 1];
    var cookieName = prefixCookie(addonKeyFromCallback(callback), name);
    if (callback._context) {
      AJS.Cookie.save(cookieName, value, expires);
    }
  },
  /**
  * Get the value of a cookie.
  * @param name {String} name of cookie to read
  * @param callback {Function} callback to pass cookie data
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  * AP.cookie.read('my_cookie', function(value) { alert(value); });
  */
  read: function read(name, callback) {
    var callback = arguments[arguments.length - 1];
    var cookieName = prefixCookie(addonKeyFromCallback(callback), name);

    var value = AJS.Cookie.read(cookieName);
    callback(value);
  },
  /**
  * Remove the given cookie.
  * @param name {String} the name of the cookie to remove
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  * AP.cookie.read('my_cookie', function(value) { alert(value); });
  * AP.cookie.erase('my_cookie');
  */
  erase: function erase(name) {
    var callback = arguments[arguments.length - 1];
    var cookieName = prefixCookie(addonKeyFromCallback(callback), name);
    AJS.Cookie.erase(cookieName);
  }
};
module.exports = exports['default'];

},{}]},{},[1])(1)
});


//# sourceMappingURL=connect-host-cookie.js.map
