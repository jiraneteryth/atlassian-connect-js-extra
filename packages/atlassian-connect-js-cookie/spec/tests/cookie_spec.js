import cookie from '../../src/host';

function resetSpies() {
  AJS.Cookie.save.calls.reset();
  AJS.Cookie.read.calls.reset();
  AJS.Cookie.erase.calls.reset();
}

const cookieReadReturnValue = 'test cookie';

describe('cookie', () => {

  beforeEach(() => {
    window.AJS.Cookie = {
      save: jasmine.createSpy('save'),
      read: jasmine.createSpy('read').and.returnValue(cookieReadReturnValue),
      erase: jasmine.createSpy('erase')
    };
  });

  describe('save', () => {
    beforeEach(() => {
      resetSpies();
    });

    it('calls AJS.Cookie with the correct arguments', () => {
      const addonKey = 'addon_key';
      const name = 'test';
      const value = 'value123';
      const expires = 123;
      const callback = {
        _context: {
          extension: {
            addon_key: addonKey
          }
        }
      };
      cookie.save(name, value, expires, callback);
      expect(AJS.Cookie.save).toHaveBeenCalledWith(`${addonKey}$$${name}`, value, expires);
    });

    it('throws an error when callback is missing', () => {
      const name = 'test';
      const value = 'value123';
      const expires = 123;
      try {
        cookie.save(name, value, expires, null);
      } catch (e) {
        expect(e.message).toEqual('addon key not found in callback');
      }
      expect(AJS.Cookie.save).not.toHaveBeenCalled();
    });

    it('throws an error when addonkey is undefined', () => {
      const name = 'test';
      const value = 'value123';
      const expires = 123;
      const callback = {
        _context: {
          extension: {}
        }
      };
      try {
        cookie.save(name, value, expires, callback);
      } catch (e) {
        expect(e.message).toEqual('addon key must be defined on cookies');
      }
      expect(AJS.Cookie.save).not.toHaveBeenCalled();
    });

    it('throws an error when cookie name is undefined', () => {
      const addonKey = 'addon_key';
      const value = 'value123';
      const expires = 123;
      const callback = {
        _context: {
          extension: {
            addon_key: addonKey
          }
        }
      };
      try {
        cookie.save(null, value, expires, callback);
      } catch (e) {
        expect(e.message).toEqual('Name must be defined');
      }
      expect(AJS.Cookie.save).not.toHaveBeenCalled();
    });
  });

  describe('read', () => {
    beforeEach(() => {
      resetSpies();
    });

    it('calls AJS.Cookie.read with the correct arugments', () => {
      const addonKey = 'addon_key';
      const name = 'test';

      const callbackSpy = jasmine.createSpy('callback spy').and.callFake((cookieValue) => {
        expect(cookieValue).toEqual(cookieReadReturnValue);
      });

      callbackSpy._context = {
        extension: {
          addon_key: addonKey
        }
      };

      cookie.read(name, callbackSpy);
      expect(AJS.Cookie.read).toHaveBeenCalledWith(`${addonKey}$$${name}`);
    });
  });

  describe('erase', () => {
    beforeEach(() => {
      resetSpies();
    });

    it('calls AJS.Cookie.erase with the correct arguments', () => {
      const addonKey = 'addon_key';
      const name = 'test';
      const callback = {
        _context: {
          extension: {
            addon_key: addonKey
          }
        }
      };

      cookie.erase(name, callback);
      expect(AJS.Cookie.erase).toHaveBeenCalledWith(`${addonKey}$$${name}`);
    });

  });
});