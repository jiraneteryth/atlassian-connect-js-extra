/**
* Allows add-ons to store, retrieve and erased cookies against the host JIRA / Confluence. These cannot be seen by other add-ons.
* @exports cookie
*/

function prefixCookie(addonKey, name) {
    if (!addonKey || addonKey.length === 0) {
        throw new Error('addon key must be defined on cookies');
    }

    if (!name || name.length === 0) {
        throw new Error('Name must be defined');
    }
    return addonKey + '$$' + name;
}

function addonKeyFromCallback(callback){
  if(callback && callback._context) {
    return callback._context.extension.addon_key;
  } else {
    throw new Error('addon key not found in callback');
  }
}

export default {
  /**
  * Save a cookie.
  * @param name {String} name of cookie
  * @param value {String} value of cookie
  * @param expires {Number} number of days before cookie expires
  * @noDemo
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  */
  save: function (name, value, expires) {
    var callback = arguments[arguments.length - 1];
    var cookieName = prefixCookie(addonKeyFromCallback(callback), name);
    if(callback._context) {
      AJS.Cookie.save(cookieName, value, expires);
    }
  },
  /**
  * Get the value of a cookie.
  * @param name {String} name of cookie to read
  * @param callback {Function} callback to pass cookie data
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  * AP.cookie.read('my_cookie', function(value) { alert(value); });
  */
  read: function (name, cb) {
    var callback = arguments[arguments.length - 1];
    var cookieName = prefixCookie(addonKeyFromCallback(callback), name);

    var value = AJS.Cookie.read(cookieName);
    cb(value);
  },
  /**
  * Remove the given cookie.
  * @param name {String} the name of the cookie to remove
  * @example
  * AP.cookie.save('my_cookie', 'my value', 1);
  * AP.cookie.read('my_cookie', function(value) { alert(value); });
  * AP.cookie.erase('my_cookie');
  */
  erase: function (name) {
    var callback = arguments[arguments.length - 1];
    var cookieName = prefixCookie(addonKeyFromCallback(callback), name);
    AJS.Cookie.erase(cookieName);
  }
};