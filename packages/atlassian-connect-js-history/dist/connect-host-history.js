(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.connectHostHistory = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
/**
* The History API allows your add-on to manipulate the current page URL for use in navigation. When using
* the history module only the page anchor is modified and not the entire window location.
*
* Note: This is only enabled for page modules (Admin page, General page, Configure page, User profile page).
* It cannot be used if the page module is launched as a dialog.
* ### Example ###
* ```
* // Register a function to run when state is changed.
* // You should use this to update your UI to show the state.
* AP.history.popState(function(e){
*     alert("The URL has changed from: " + e.oldURL + "to: " + e.newURL);
* });
*
* // Adds a new entry to the history and changes the url in the browser.
* AP.history.pushState("page2");
*
* // Changes the URL back and invokes any registered popState callbacks.
* AP.history.back();
* ```
* @exports history
*/

"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var listeners = [],
    lastAdded;

var ANCHOR_PREFIX = "!";

function anchorFromUrl(url) {
  if (typeof url !== "string" || url.search("#") === -1) {
    return "";
  }
  return stripPrefix(stripHash(url));
}

function stripHash(str) {
  return str.slice(str.search("#") + 1);
}

function stripPrefix(text) {
  if (text === undefined || text === null || text === "") {
    return "";
  }
  return text.toString().replace(new RegExp("^" + ANCHOR_PREFIX), "");
}

function addPrefix(text) {
  if (text === undefined || text === null) {
    throw "You must supply text to prefix";
  }
  return ANCHOR_PREFIX + stripPrefix(text);
}

function hashChange(event) {
  var response;
  var newUrlAnchor = anchorFromUrl(event.originalEvent.newURL);
  var oldUrlAnchor = anchorFromUrl(event.originalEvent.oldURL);
  if (newUrlAnchor !== oldUrlAnchor && // if the url has changed
  lastAdded !== newUrlAnchor //  and it's not the page we just pushed.
  ) {
      response = sanitizeHashChangeEvent(event.originalEvent);
    } else {
    response = false;
  }
  lastAdded = null;
  return response;
}

function sanitizeHashChangeEvent(e) {
  return {
    newURL: anchorFromUrl(e.newURL),
    oldURL: anchorFromUrl(e.oldURL)
  };
}

function callConnectHost(cb) {
  if (_dereq_ && _dereq_.amd) {
    _dereq_(['connect-host'], function (connectHost) {
      cb(connectHost);
    });
  } else {
    cb(window.connectHost);
  }
}

var hashChangeListenerRegistered = false;

function registerHashChangeListener() {
  if (hashChangeListenerRegistered) {
    return;
  }
  AJS.$(window).on("hashchange", function (e) {
    var event = hashChange(e);
    if (event) {
      callConnectHost(function (ch) {
        ch.broadcastEvent('history_popstate', {}, event);
      });
    }
  });
  hashChangeListenerRegistered = true;
}

function changeState(anchor, replace) {
  var currentUrlAnchor = anchorFromUrl(window.location.href),
      newUrl = window.location.href.replace(window.location.hash, ""),
      newUrlAnchor = addPrefix(anchor);

  // If the url has changed.
  if (anchor !== currentUrlAnchor) {
    lastAdded = stripPrefix(newUrlAnchor);
    // If it was replaceState or pushState?
    if (replace) {
      window.location.replace("#" + newUrlAnchor);
    } else {
      window.location.assign("#" + newUrlAnchor);
    }
    return newUrlAnchor;
  }
}

exports["default"] = {
  /**
  * Goes back one step in the joint session history. Will invoke the popState callback.
  * @noDemo
  * @example
  * AP.history.back(); // go back by 1 entry in the browser history.
  */
  back: function back(callback) {
    callback = arguments[arguments.length - 1];
    this.go(-1, callback);
  },
  /**
  * Goes back one step in the joint session history. Will invoke the popState callback.
  * @noDemo
  * @example
  * AP.history.forward(); // go forward by 1 entry in the browser history.
  */
  forward: function forward(callback) {
    callback = arguments[arguments.length - 1];
    this.go(1, callback);
  },
  /**
  * Retrieves the current state of the history stack and returns the value. The returned value is the same as what was set with the pushState method.
  * @return {String} The current url anchor
  * @noDemo
  * @example
  * AP.history.pushState("page5");
  * AP.history.getState(); // returns "page5";
  */
  getState: function getState(callback) {
    callback = arguments[arguments.length - 1];
    callback(stripPrefix(stripHash(window.location.hash)));
  },
  /**
  * Moves the page history back or forward the specified number of steps. A zero delta will reload the current page.
  * If the delta is out of range, does nothing. This call invoke the popState callback.
  * @param {Integer} delta - Number of places to move in the history
  * @noDemo
  * @example
  * AP.history.go(-2); // go back by 2 entries in the browser history.
  */
  go: function go(delta) {
    var callback = arguments[arguments.length - 1];
    if (callback._context.extension.options.isFullPage) {
      window.history.go(delta);
    } else {
      AJS.log("History is only available to page modules");
    }
  },
  _registerPopStateListener: function _registerPopStateListener() {
    registerHashChangeListener();
  },
  /**
  * Updates the location's anchor with the specified value and pushes the given data onto the session history.
  * Does not invoke popState callback.
  * @param {String} url - URL to add to history
  */
  pushState: function pushState(newState, callback) {
    callback = arguments[arguments.length - 1];
    if (callback._context.extension.options.isFullPage) {
      changeState(newState);
    } else {
      AJS.log("History is only available to page modules");
    }
  },
  /**
  * Updates the current entry in the session history. Updates the location's anchor with the specified value but does not change the session history. Does not invoke popState callback.
  * @param {String} url - URL to update current history value with
  */
  replaceState: function replaceState(newState, callback) {
    callback = arguments[arguments.length - 1];
    if (callback._context.extension.options.isFullPage) {
      changeState(newState, true);
    } else {
      AJS.log("History is only available to page modules");
    }
  }

  /**
   * Register a function to be executed on state change.
   * @name popState
   * @method   
   * @param {Function} callback - Callback method to be executed on state change.
   */
};
module.exports = exports["default"];

},{}]},{},[1])(1)
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvVXNlcnMva2hhbmd1eWVuL3NyYy9hdGxhc3NpYW4tY29ubmVjdC1qcy1leHRyYS9wYWNrYWdlcy9hdGxhc3NpYW4tY29ubmVjdC1qcy1oaXN0b3J5L3NyYy9ob3N0L2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ3VCQSxJQUFJLFNBQVMsR0FBRyxFQUFFO0lBQ2QsU0FBUyxDQUFDOztBQUVkLElBQU0sYUFBYSxHQUFHLEdBQUcsQ0FBQzs7QUFFMUIsU0FBUyxhQUFhLENBQUMsR0FBRyxFQUFFO0FBQzFCLE1BQUcsT0FBTyxHQUFHLEtBQUssUUFBUSxJQUFJLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7QUFDcEQsV0FBTyxFQUFFLENBQUM7R0FDWDtBQUNELFNBQU8sV0FBVyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0NBQ3BDOztBQUVELFNBQVMsU0FBUyxDQUFDLEdBQUcsRUFBRTtBQUN0QixTQUFPLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQTtDQUN0Qzs7QUFFRCxTQUFTLFdBQVcsQ0FBRSxJQUFJLEVBQUU7QUFDeEIsTUFBRyxJQUFJLEtBQUssU0FBUyxJQUFJLElBQUksS0FBSyxJQUFJLElBQUksSUFBSSxLQUFLLEVBQUUsRUFBQztBQUNsRCxXQUFPLEVBQUUsQ0FBQztHQUNiO0FBQ0QsU0FBTyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsT0FBTyxDQUFDLElBQUksTUFBTSxDQUFDLEdBQUcsR0FBRyxhQUFhLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztDQUN2RTs7QUFFRCxTQUFTLFNBQVMsQ0FBRSxJQUFJLEVBQUU7QUFDdEIsTUFBRyxJQUFJLEtBQUssU0FBUyxJQUFJLElBQUksS0FBSyxJQUFJLEVBQUM7QUFDbkMsVUFBTSxnQ0FBZ0MsQ0FBQztHQUMxQztBQUNELFNBQU8sYUFBYSxHQUFHLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztDQUM1Qzs7QUFFRCxTQUFTLFVBQVUsQ0FBRSxLQUFLLEVBQUU7QUFDMUIsTUFBSSxRQUFRLENBQUM7QUFDYixNQUFJLFlBQVksR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM3RCxNQUFJLFlBQVksR0FBRyxhQUFhLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM3RCxNQUFJLEFBQUUsWUFBWSxLQUFLLFlBQVk7QUFDN0IsV0FBUyxLQUFLLFlBQVksQUFBRTtJQUNoQztBQUNBLGNBQVEsR0FBRyx1QkFBdUIsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLENBQUM7S0FDekQsTUFBTTtBQUNMLFlBQVEsR0FBRyxLQUFLLENBQUM7R0FDbEI7QUFDRCxXQUFTLEdBQUcsSUFBSSxDQUFDO0FBQ2pCLFNBQU8sUUFBUSxDQUFDO0NBQ2pCOztBQUVELFNBQVMsdUJBQXVCLENBQUUsQ0FBQyxFQUFFO0FBQ25DLFNBQU87QUFDTCxVQUFNLEVBQUUsYUFBYSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7QUFDL0IsVUFBTSxFQUFFLGFBQWEsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDO0dBQ2hDLENBQUM7Q0FDSDs7QUFFRCxTQUFTLGVBQWUsQ0FBQyxFQUFFLEVBQUM7QUFDMUIsTUFBRyxPQUFPLElBQUksT0FBTyxDQUFDLEdBQUcsRUFBQztBQUN4QixXQUFPLENBQUMsQ0FBQyxjQUFjLENBQUMsRUFBRSxVQUFTLFdBQVcsRUFBQztBQUM3QyxRQUFFLENBQUMsV0FBVyxDQUFDLENBQUM7S0FDakIsQ0FBQyxDQUFDO0dBQ0osTUFBTTtBQUNMLE1BQUUsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUM7R0FDeEI7Q0FDRjs7QUFFRCxJQUFJLDRCQUE0QixHQUFHLEtBQUssQ0FBQzs7QUFFekMsU0FBUywwQkFBMEIsR0FBRTtBQUNuQyxNQUFHLDRCQUE0QixFQUFFO0FBQy9CLFdBQU87R0FDUjtBQUNELEtBQUcsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBRSxVQUFTLENBQUMsRUFBQztBQUN4QyxRQUFJLEtBQUssR0FBRyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7QUFDMUIsUUFBRyxLQUFLLEVBQUU7QUFDUixxQkFBZSxDQUFDLFVBQVMsRUFBRSxFQUFDO0FBQzFCLFVBQUUsQ0FBQyxjQUFjLENBQUMsa0JBQWtCLEVBQUUsRUFBRSxFQUFFLEtBQUssQ0FBQyxDQUFDO09BQ2xELENBQUMsQ0FBQztLQUNKO0dBQ0YsQ0FBQyxDQUFDO0FBQ0gsOEJBQTRCLEdBQUcsSUFBSSxDQUFDO0NBQ3JDOztBQUVELFNBQVMsV0FBVyxDQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUU7QUFDbkMsTUFBSSxnQkFBZ0IsR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7TUFDMUQsTUFBTSxHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksRUFBQyxFQUFFLENBQUM7TUFDOUQsWUFBWSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQzs7O0FBR2pDLE1BQUcsTUFBTSxLQUFLLGdCQUFnQixFQUFDO0FBQzNCLGFBQVMsR0FBRyxXQUFXLENBQUMsWUFBWSxDQUFDLENBQUM7O0FBRXRDLFFBQUcsT0FBTyxFQUFDO0FBQ1AsWUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxHQUFHLFlBQVksQ0FBQyxDQUFDO0tBQy9DLE1BQU07QUFDSCxZQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEdBQUcsWUFBWSxDQUFDLENBQUM7S0FDOUM7QUFDRCxXQUFPLFlBQVksQ0FBQztHQUN2QjtDQUNKOztxQkFFYzs7Ozs7OztBQU9iLE1BQUksRUFBRSxjQUFTLFFBQVEsRUFBQztBQUN0QixZQUFRLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7QUFDM0MsUUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxRQUFRLENBQUMsQ0FBQztHQUN2Qjs7Ozs7OztBQU9ELFNBQU8sRUFBRSxpQkFBUyxRQUFRLEVBQUM7QUFDekIsWUFBUSxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO0FBQzNDLFFBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLFFBQVEsQ0FBQyxDQUFDO0dBQ3RCOzs7Ozs7Ozs7QUFTRCxVQUFRLEVBQUUsa0JBQVMsUUFBUSxFQUFDO0FBQzFCLFlBQVEsR0FBRyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztBQUMzQyxZQUFRLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQztHQUN4RDs7Ozs7Ozs7O0FBU0QsSUFBRSxFQUFFLFlBQVMsS0FBSyxFQUFDO0FBQ2pCLFFBQUksUUFBUSxHQUFHLFNBQVMsQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO0FBQy9DLFFBQUcsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRTtBQUMvQyxZQUFNLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztLQUM1QixNQUFNO0FBQ0gsU0FBRyxDQUFDLEdBQUcsQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDO0tBQ3hEO0dBQ0Y7QUFDRCwyQkFBeUIsRUFBRSxxQ0FBVTtBQUNqQyw4QkFBMEIsRUFBRSxDQUFDO0dBQ2hDOzs7Ozs7QUFNRCxXQUFTLEVBQUUsbUJBQVMsUUFBUSxFQUFFLFFBQVEsRUFBQztBQUNyQyxZQUFRLEdBQUcsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7QUFDM0MsUUFBRyxRQUFRLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFO0FBQy9DLGlCQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7S0FDekIsTUFBTTtBQUNILFNBQUcsQ0FBQyxHQUFHLENBQUMsMkNBQTJDLENBQUMsQ0FBQztLQUN4RDtHQUNGOzs7OztBQUtELGNBQVksRUFBRSxzQkFBUyxRQUFRLEVBQUUsUUFBUSxFQUFFO0FBQ3pDLFlBQVEsR0FBRyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztBQUMzQyxRQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUU7QUFDL0MsaUJBQVcsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUM7S0FDL0IsTUFBTTtBQUNILFNBQUcsQ0FBQyxHQUFHLENBQUMsMkNBQTJDLENBQUMsQ0FBQztLQUN4RDtHQUNGOzs7Ozs7OztDQVFGIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8qKlxuKiBUaGUgSGlzdG9yeSBBUEkgYWxsb3dzIHlvdXIgYWRkLW9uIHRvIG1hbmlwdWxhdGUgdGhlIGN1cnJlbnQgcGFnZSBVUkwgZm9yIHVzZSBpbiBuYXZpZ2F0aW9uLiBXaGVuIHVzaW5nXG4qIHRoZSBoaXN0b3J5IG1vZHVsZSBvbmx5IHRoZSBwYWdlIGFuY2hvciBpcyBtb2RpZmllZCBhbmQgbm90IHRoZSBlbnRpcmUgd2luZG93IGxvY2F0aW9uLlxuKlxuKiBOb3RlOiBUaGlzIGlzIG9ubHkgZW5hYmxlZCBmb3IgcGFnZSBtb2R1bGVzIChBZG1pbiBwYWdlLCBHZW5lcmFsIHBhZ2UsIENvbmZpZ3VyZSBwYWdlLCBVc2VyIHByb2ZpbGUgcGFnZSkuXG4qIEl0IGNhbm5vdCBiZSB1c2VkIGlmIHRoZSBwYWdlIG1vZHVsZSBpcyBsYXVuY2hlZCBhcyBhIGRpYWxvZy5cbiogIyMjIEV4YW1wbGUgIyMjXG4qIGBgYFxuKiAvLyBSZWdpc3RlciBhIGZ1bmN0aW9uIHRvIHJ1biB3aGVuIHN0YXRlIGlzIGNoYW5nZWQuXG4qIC8vIFlvdSBzaG91bGQgdXNlIHRoaXMgdG8gdXBkYXRlIHlvdXIgVUkgdG8gc2hvdyB0aGUgc3RhdGUuXG4qIEFQLmhpc3RvcnkucG9wU3RhdGUoZnVuY3Rpb24oZSl7XG4qICAgICBhbGVydChcIlRoZSBVUkwgaGFzIGNoYW5nZWQgZnJvbTogXCIgKyBlLm9sZFVSTCArIFwidG86IFwiICsgZS5uZXdVUkwpO1xuKiB9KTtcbipcbiogLy8gQWRkcyBhIG5ldyBlbnRyeSB0byB0aGUgaGlzdG9yeSBhbmQgY2hhbmdlcyB0aGUgdXJsIGluIHRoZSBicm93c2VyLlxuKiBBUC5oaXN0b3J5LnB1c2hTdGF0ZShcInBhZ2UyXCIpO1xuKlxuKiAvLyBDaGFuZ2VzIHRoZSBVUkwgYmFjayBhbmQgaW52b2tlcyBhbnkgcmVnaXN0ZXJlZCBwb3BTdGF0ZSBjYWxsYmFja3MuXG4qIEFQLmhpc3RvcnkuYmFjaygpO1xuKiBgYGBcbiogQGV4cG9ydHMgaGlzdG9yeVxuKi9cblxudmFyIGxpc3RlbmVycyA9IFtdLFxuICAgIGxhc3RBZGRlZDtcblxuY29uc3QgQU5DSE9SX1BSRUZJWCA9IFwiIVwiO1xuXG5mdW5jdGlvbiBhbmNob3JGcm9tVXJsKHVybCkge1xuICBpZih0eXBlb2YgdXJsICE9PSBcInN0cmluZ1wiIHx8IHVybC5zZWFyY2goXCIjXCIpID09PSAtMSkge1xuICAgIHJldHVybiBcIlwiO1xuICB9XG4gIHJldHVybiBzdHJpcFByZWZpeChzdHJpcEhhc2godXJsKSk7XG59XG5cbmZ1bmN0aW9uIHN0cmlwSGFzaChzdHIpIHtcbiAgcmV0dXJuIHN0ci5zbGljZShzdHIuc2VhcmNoKFwiI1wiKSArIDEpXG59XG5cbmZ1bmN0aW9uIHN0cmlwUHJlZml4ICh0ZXh0KSB7XG4gICAgaWYodGV4dCA9PT0gdW5kZWZpbmVkIHx8IHRleHQgPT09IG51bGwgfHwgdGV4dCA9PT0gXCJcIil7XG4gICAgICAgIHJldHVybiBcIlwiO1xuICAgIH1cbiAgICByZXR1cm4gdGV4dC50b1N0cmluZygpLnJlcGxhY2UobmV3IFJlZ0V4cChcIl5cIiArIEFOQ0hPUl9QUkVGSVgpLCBcIlwiKTtcbn1cblxuZnVuY3Rpb24gYWRkUHJlZml4ICh0ZXh0KSB7XG4gICAgaWYodGV4dCA9PT0gdW5kZWZpbmVkIHx8IHRleHQgPT09IG51bGwpe1xuICAgICAgICB0aHJvdyBcIllvdSBtdXN0IHN1cHBseSB0ZXh0IHRvIHByZWZpeFwiO1xuICAgIH1cbiAgICByZXR1cm4gQU5DSE9SX1BSRUZJWCArIHN0cmlwUHJlZml4KHRleHQpO1xufVxuXG5mdW5jdGlvbiBoYXNoQ2hhbmdlIChldmVudCkge1xuICB2YXIgcmVzcG9uc2U7XG4gIHZhciBuZXdVcmxBbmNob3IgPSBhbmNob3JGcm9tVXJsKGV2ZW50Lm9yaWdpbmFsRXZlbnQubmV3VVJMKTtcbiAgdmFyIG9sZFVybEFuY2hvciA9IGFuY2hvckZyb21VcmwoZXZlbnQub3JpZ2luYWxFdmVudC5vbGRVUkwpO1xuICBpZiggKCBuZXdVcmxBbmNob3IgIT09IG9sZFVybEFuY2hvcikgJiYgLy8gaWYgdGhlIHVybCBoYXMgY2hhbmdlZFxuICAgICAgKCBsYXN0QWRkZWQgIT09IG5ld1VybEFuY2hvciApIC8vICBhbmQgaXQncyBub3QgdGhlIHBhZ2Ugd2UganVzdCBwdXNoZWQuXG4gICApe1xuICAgIHJlc3BvbnNlID0gc2FuaXRpemVIYXNoQ2hhbmdlRXZlbnQoZXZlbnQub3JpZ2luYWxFdmVudCk7XG4gIH0gZWxzZSB7XG4gICAgcmVzcG9uc2UgPSBmYWxzZTtcbiAgfVxuICBsYXN0QWRkZWQgPSBudWxsO1xuICByZXR1cm4gcmVzcG9uc2U7XG59XG5cbmZ1bmN0aW9uIHNhbml0aXplSGFzaENoYW5nZUV2ZW50IChlKSB7XG4gIHJldHVybiB7XG4gICAgbmV3VVJMOiBhbmNob3JGcm9tVXJsKGUubmV3VVJMKSxcbiAgICBvbGRVUkw6IGFuY2hvckZyb21VcmwoZS5vbGRVUkwpXG4gIH07XG59XG5cbmZ1bmN0aW9uIGNhbGxDb25uZWN0SG9zdChjYil7XG4gIGlmKHJlcXVpcmUgJiYgcmVxdWlyZS5hbWQpe1xuICAgIHJlcXVpcmUoWydjb25uZWN0LWhvc3QnXSwgZnVuY3Rpb24oY29ubmVjdEhvc3Qpe1xuICAgICAgY2IoY29ubmVjdEhvc3QpO1xuICAgIH0pO1xuICB9IGVsc2Uge1xuICAgIGNiKHdpbmRvdy5jb25uZWN0SG9zdCk7XG4gIH1cbn1cblxudmFyIGhhc2hDaGFuZ2VMaXN0ZW5lclJlZ2lzdGVyZWQgPSBmYWxzZTtcblxuZnVuY3Rpb24gcmVnaXN0ZXJIYXNoQ2hhbmdlTGlzdGVuZXIoKXtcbiAgaWYoaGFzaENoYW5nZUxpc3RlbmVyUmVnaXN0ZXJlZCkge1xuICAgIHJldHVybjtcbiAgfVxuICBBSlMuJCh3aW5kb3cpLm9uKFwiaGFzaGNoYW5nZVwiLCBmdW5jdGlvbihlKXtcbiAgICB2YXIgZXZlbnQgPSBoYXNoQ2hhbmdlKGUpO1xuICAgIGlmKGV2ZW50KSB7XG4gICAgICBjYWxsQ29ubmVjdEhvc3QoZnVuY3Rpb24oY2gpe1xuICAgICAgICBjaC5icm9hZGNhc3RFdmVudCgnaGlzdG9yeV9wb3BzdGF0ZScsIHt9LCBldmVudCk7XG4gICAgICB9KTtcbiAgICB9XG4gIH0pO1xuICBoYXNoQ2hhbmdlTGlzdGVuZXJSZWdpc3RlcmVkID0gdHJ1ZTtcbn1cblxuZnVuY3Rpb24gY2hhbmdlU3RhdGUgKGFuY2hvciwgcmVwbGFjZSkge1xuICAgIHZhciBjdXJyZW50VXJsQW5jaG9yID0gYW5jaG9yRnJvbVVybCh3aW5kb3cubG9jYXRpb24uaHJlZiksXG4gICAgbmV3VXJsID0gd2luZG93LmxvY2F0aW9uLmhyZWYucmVwbGFjZSh3aW5kb3cubG9jYXRpb24uaGFzaCxcIlwiKSxcbiAgICBuZXdVcmxBbmNob3IgPSBhZGRQcmVmaXgoYW5jaG9yKTtcblxuICAgIC8vIElmIHRoZSB1cmwgaGFzIGNoYW5nZWQuXG4gICAgaWYoYW5jaG9yICE9PSBjdXJyZW50VXJsQW5jaG9yKXtcbiAgICAgICAgbGFzdEFkZGVkID0gc3RyaXBQcmVmaXgobmV3VXJsQW5jaG9yKTtcbiAgICAgICAgLy8gSWYgaXQgd2FzIHJlcGxhY2VTdGF0ZSBvciBwdXNoU3RhdGU/XG4gICAgICAgIGlmKHJlcGxhY2Upe1xuICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLnJlcGxhY2UoXCIjXCIgKyBuZXdVcmxBbmNob3IpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLmFzc2lnbihcIiNcIiArIG5ld1VybEFuY2hvcik7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG5ld1VybEFuY2hvcjtcbiAgICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgLyoqXG4gICogR29lcyBiYWNrIG9uZSBzdGVwIGluIHRoZSBqb2ludCBzZXNzaW9uIGhpc3RvcnkuIFdpbGwgaW52b2tlIHRoZSBwb3BTdGF0ZSBjYWxsYmFjay5cbiAgKiBAbm9EZW1vXG4gICogQGV4YW1wbGVcbiAgKiBBUC5oaXN0b3J5LmJhY2soKTsgLy8gZ28gYmFjayBieSAxIGVudHJ5IGluIHRoZSBicm93c2VyIGhpc3RvcnkuXG4gICovXG4gIGJhY2s6IGZ1bmN0aW9uKGNhbGxiYWNrKXtcbiAgICBjYWxsYmFjayA9IGFyZ3VtZW50c1thcmd1bWVudHMubGVuZ3RoIC0gMV07XG4gICAgdGhpcy5nbygtMSwgY2FsbGJhY2spO1xuICB9LFxuICAvKipcbiAgKiBHb2VzIGJhY2sgb25lIHN0ZXAgaW4gdGhlIGpvaW50IHNlc3Npb24gaGlzdG9yeS4gV2lsbCBpbnZva2UgdGhlIHBvcFN0YXRlIGNhbGxiYWNrLlxuICAqIEBub0RlbW9cbiAgKiBAZXhhbXBsZVxuICAqIEFQLmhpc3RvcnkuZm9yd2FyZCgpOyAvLyBnbyBmb3J3YXJkIGJ5IDEgZW50cnkgaW4gdGhlIGJyb3dzZXIgaGlzdG9yeS5cbiAgKi9cbiAgZm9yd2FyZDogZnVuY3Rpb24oY2FsbGJhY2spe1xuICAgIGNhbGxiYWNrID0gYXJndW1lbnRzW2FyZ3VtZW50cy5sZW5ndGggLSAxXTtcbiAgICB0aGlzLmdvKDEsIGNhbGxiYWNrKTtcbiAgfSxcbiAgLyoqXG4gICogUmV0cmlldmVzIHRoZSBjdXJyZW50IHN0YXRlIG9mIHRoZSBoaXN0b3J5IHN0YWNrIGFuZCByZXR1cm5zIHRoZSB2YWx1ZS4gVGhlIHJldHVybmVkIHZhbHVlIGlzIHRoZSBzYW1lIGFzIHdoYXQgd2FzIHNldCB3aXRoIHRoZSBwdXNoU3RhdGUgbWV0aG9kLlxuICAqIEByZXR1cm4ge1N0cmluZ30gVGhlIGN1cnJlbnQgdXJsIGFuY2hvclxuICAqIEBub0RlbW9cbiAgKiBAZXhhbXBsZVxuICAqIEFQLmhpc3RvcnkucHVzaFN0YXRlKFwicGFnZTVcIik7XG4gICogQVAuaGlzdG9yeS5nZXRTdGF0ZSgpOyAvLyByZXR1cm5zIFwicGFnZTVcIjtcbiAgKi9cbiAgZ2V0U3RhdGU6IGZ1bmN0aW9uKGNhbGxiYWNrKXtcbiAgICBjYWxsYmFjayA9IGFyZ3VtZW50c1thcmd1bWVudHMubGVuZ3RoIC0gMV07XG4gICAgY2FsbGJhY2soc3RyaXBQcmVmaXgoc3RyaXBIYXNoKHdpbmRvdy5sb2NhdGlvbi5oYXNoKSkpO1xuICB9LFxuICAvKipcbiAgKiBNb3ZlcyB0aGUgcGFnZSBoaXN0b3J5IGJhY2sgb3IgZm9yd2FyZCB0aGUgc3BlY2lmaWVkIG51bWJlciBvZiBzdGVwcy4gQSB6ZXJvIGRlbHRhIHdpbGwgcmVsb2FkIHRoZSBjdXJyZW50IHBhZ2UuXG4gICogSWYgdGhlIGRlbHRhIGlzIG91dCBvZiByYW5nZSwgZG9lcyBub3RoaW5nLiBUaGlzIGNhbGwgaW52b2tlIHRoZSBwb3BTdGF0ZSBjYWxsYmFjay5cbiAgKiBAcGFyYW0ge0ludGVnZXJ9IGRlbHRhIC0gTnVtYmVyIG9mIHBsYWNlcyB0byBtb3ZlIGluIHRoZSBoaXN0b3J5XG4gICogQG5vRGVtb1xuICAqIEBleGFtcGxlXG4gICogQVAuaGlzdG9yeS5nbygtMik7IC8vIGdvIGJhY2sgYnkgMiBlbnRyaWVzIGluIHRoZSBicm93c2VyIGhpc3RvcnkuXG4gICovXG4gIGdvOiBmdW5jdGlvbihkZWx0YSl7XG4gICAgdmFyIGNhbGxiYWNrID0gYXJndW1lbnRzW2FyZ3VtZW50cy5sZW5ndGggLSAxXTtcbiAgICBpZihjYWxsYmFjay5fY29udGV4dC5leHRlbnNpb24ub3B0aW9ucy5pc0Z1bGxQYWdlKSB7XG4gICAgICAgIHdpbmRvdy5oaXN0b3J5LmdvKGRlbHRhKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBBSlMubG9nKFwiSGlzdG9yeSBpcyBvbmx5IGF2YWlsYWJsZSB0byBwYWdlIG1vZHVsZXNcIik7XG4gICAgfVxuICB9LFxuICBfcmVnaXN0ZXJQb3BTdGF0ZUxpc3RlbmVyOiBmdW5jdGlvbigpe1xuICAgICAgcmVnaXN0ZXJIYXNoQ2hhbmdlTGlzdGVuZXIoKTtcbiAgfSxcbiAgLyoqXG4gICogVXBkYXRlcyB0aGUgbG9jYXRpb24ncyBhbmNob3Igd2l0aCB0aGUgc3BlY2lmaWVkIHZhbHVlIGFuZCBwdXNoZXMgdGhlIGdpdmVuIGRhdGEgb250byB0aGUgc2Vzc2lvbiBoaXN0b3J5LlxuICAqIERvZXMgbm90IGludm9rZSBwb3BTdGF0ZSBjYWxsYmFjay5cbiAgKiBAcGFyYW0ge1N0cmluZ30gdXJsIC0gVVJMIHRvIGFkZCB0byBoaXN0b3J5XG4gICovXG4gIHB1c2hTdGF0ZTogZnVuY3Rpb24obmV3U3RhdGUsIGNhbGxiYWNrKXtcbiAgICBjYWxsYmFjayA9IGFyZ3VtZW50c1thcmd1bWVudHMubGVuZ3RoIC0gMV07XG4gICAgaWYoY2FsbGJhY2suX2NvbnRleHQuZXh0ZW5zaW9uLm9wdGlvbnMuaXNGdWxsUGFnZSkge1xuICAgICAgICBjaGFuZ2VTdGF0ZShuZXdTdGF0ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgQUpTLmxvZyhcIkhpc3RvcnkgaXMgb25seSBhdmFpbGFibGUgdG8gcGFnZSBtb2R1bGVzXCIpO1xuICAgIH1cbiAgfSxcbiAgLyoqXG4gICogVXBkYXRlcyB0aGUgY3VycmVudCBlbnRyeSBpbiB0aGUgc2Vzc2lvbiBoaXN0b3J5LiBVcGRhdGVzIHRoZSBsb2NhdGlvbidzIGFuY2hvciB3aXRoIHRoZSBzcGVjaWZpZWQgdmFsdWUgYnV0IGRvZXMgbm90IGNoYW5nZSB0aGUgc2Vzc2lvbiBoaXN0b3J5LiBEb2VzIG5vdCBpbnZva2UgcG9wU3RhdGUgY2FsbGJhY2suXG4gICogQHBhcmFtIHtTdHJpbmd9IHVybCAtIFVSTCB0byB1cGRhdGUgY3VycmVudCBoaXN0b3J5IHZhbHVlIHdpdGhcbiAgKi9cbiAgcmVwbGFjZVN0YXRlOiBmdW5jdGlvbihuZXdTdGF0ZSwgY2FsbGJhY2spIHtcbiAgICBjYWxsYmFjayA9IGFyZ3VtZW50c1thcmd1bWVudHMubGVuZ3RoIC0gMV07XG4gICAgaWYoY2FsbGJhY2suX2NvbnRleHQuZXh0ZW5zaW9uLm9wdGlvbnMuaXNGdWxsUGFnZSkge1xuICAgICAgICBjaGFuZ2VTdGF0ZShuZXdTdGF0ZSwgdHJ1ZSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgQUpTLmxvZyhcIkhpc3RvcnkgaXMgb25seSBhdmFpbGFibGUgdG8gcGFnZSBtb2R1bGVzXCIpO1xuICAgIH1cbiAgfVxuXG4gIC8qKlxuICAgKiBSZWdpc3RlciBhIGZ1bmN0aW9uIHRvIGJlIGV4ZWN1dGVkIG9uIHN0YXRlIGNoYW5nZS5cbiAgICogQG5hbWUgcG9wU3RhdGVcbiAgICogQG1ldGhvZCAgIFxuICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFjayAtIENhbGxiYWNrIG1ldGhvZCB0byBiZSBleGVjdXRlZCBvbiBzdGF0ZSBjaGFuZ2UuXG4gICAqL1xufTsiXX0=
