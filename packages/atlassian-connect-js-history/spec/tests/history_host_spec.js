import history from '../../src/host';

const defaultContext = {
  extension: {
    options: {
      isFullPage: true
    }
  }
};

const notFullPageContext = {
  extension: {
    options: {
      isFullPage: false
    }
  }
};

describe('history', () => {
  describe('back', () => {
    it('calls history.go', () => {
      spyOn(history, 'go');
      const callback = {
        _context: defaultContext
      };
      history.back(callback);
      expect(history.go).toHaveBeenCalledWith(-1, callback);
    });
  });

  describe('forward', () => {
    it('calls history.go', () => {
      spyOn(history, 'go');
      const callback = {
        _context: defaultContext
      };
      history.forward(callback);
      expect(history.go).toHaveBeenCalledWith(1, callback);
    });
  });

  describe('getState', () => {
    it('returns the current state', () => {
      const newState = 'new-state';
      history.pushState(newState, {
        _context: defaultContext
      });
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;
      expect(window.location.hash).toEqual(`#!${newState}`);

      history.getState(callback)
      expect(callback).toHaveBeenCalledWith(newState);
    });
  });

  describe('go', () => {
    it('does nothing when not on a page module', () => {
      spyOn(window.history, 'go');
      history.go(-2, {
        _context: notFullPageContext
      });
      expect(window.history.go).not.toHaveBeenCalled();
    });

    it('calls window.history.go and the provided callback', () => {
      spyOn(window.history, 'go');
      const delta = -2;
      history.go(delta, {
        _context: defaultContext
      });
      expect(window.history.go).toHaveBeenCalledWith(delta);
    });
  });

  describe('pushState', () => {
    it('does nothing when not on a page module',  () => {
      const currentHash = window.location.hash;
      history.pushState('some-new-hash', {
        _context: notFullPageContext
      });
      expect(window.location.hash).toEqual(currentHash);
    });

    it('updates the locations anchor and changes the session history',  () => {      
      const newState = 'new-state';
      const callback = {
        _context: defaultContext
      };
      history.pushState(newState, callback);
      expect(window.location.hash).toEqual(`#!${newState}`);
    });

    it('does nothing if new state is the same as the current state',  () => {
      const originalHistoryLength = window.history.length;
      const newState = 'new-state';
      const callback = {
        _context: defaultContext
      };
      window.location.assign(`#!${newState}`);
      history.pushState(newState, callback);
      expect(window.location.hash).toEqual(`#!${newState}`);
      expect(window.history.length).toEqual(originalHistoryLength);
    });
  });

  describe('replaceState', () => {
    it('does nothing when not on a page module',  () => {
      const currentHash = window.location.hash;
      history.replaceState('some-new-hash', {
        _context: notFullPageContext
      });
      expect(window.location.hash).toEqual(currentHash);
    });

    it('updates locations anchor but does not change the session history',  () => {
      const originalHistoryLength = window.history.length;
      const newState = 'new-state';
      const callback = {
        _context: defaultContext
      };
      window.location.assign(`#!${newState}`);
      history.replaceState(newState, callback);
      expect(window.location.hash).toEqual(`#!${newState}`);
      expect(window.history.length).toEqual(originalHistoryLength);
    });
  });

});