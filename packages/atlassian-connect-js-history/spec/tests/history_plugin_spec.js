describe('history plugin', () => {
  const historyStub = {
    _registerPopStateListener: jasmine.createSpy('register pop state listener')
  }

  const AP = {
    _hostModules: {
      history: historyStub
    },
    register: jasmine.createSpy('register'),
    history: historyStub
  }

  window.AP = AP;
  require('../../src/plugin/index');

  beforeEach(() => {
    window.AP = AP;
  })

  it('calls AP.register', () => {
    expect(AP.register).toHaveBeenCalled();
  });

  it('sets popState', () => {
    expect(AP._hostModules.history.popState).toBeDefined();
    expect(AP.history.popState).toBeDefined();
  });

  it('calls _registerPopStateListener', () => {
    AP.history.popState();
    expect(AP.history._registerPopStateListener).toHaveBeenCalled();
  });
});