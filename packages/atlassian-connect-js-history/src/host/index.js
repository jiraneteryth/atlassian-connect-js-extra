/**
* The History API allows your add-on to manipulate the current page URL for use in navigation. When using
* the history module only the page anchor is modified and not the entire window location.
*
* Note: This is only enabled for page modules (Admin page, General page, Configure page, User profile page).
* It cannot be used if the page module is launched as a dialog.
* ### Example ###
* ```
* // Register a function to run when state is changed.
* // You should use this to update your UI to show the state.
* AP.history.popState(function(e){
*     alert("The URL has changed from: " + e.oldURL + "to: " + e.newURL);
* });
*
* // Adds a new entry to the history and changes the url in the browser.
* AP.history.pushState("page2");
*
* // Changes the URL back and invokes any registered popState callbacks.
* AP.history.back();
* ```
* @exports history
*/

var listeners = [],
    lastAdded;

const ANCHOR_PREFIX = "!";

function anchorFromUrl(url) {
  if(typeof url !== "string" || url.search("#") === -1) {
    return "";
  }
  return stripPrefix(stripHash(url));
}

function stripHash(str) {
  return str.slice(str.search("#") + 1)
}

function stripPrefix (text) {
    if(text === undefined || text === null || text === ""){
        return "";
    }
    return text.toString().replace(new RegExp("^" + ANCHOR_PREFIX), "");
}

function addPrefix (text) {
    if(text === undefined || text === null){
        throw "You must supply text to prefix";
    }
    return ANCHOR_PREFIX + stripPrefix(text);
}

function hashChange (event) {
  var response;
  var newUrlAnchor = anchorFromUrl(event.originalEvent.newURL);
  var oldUrlAnchor = anchorFromUrl(event.originalEvent.oldURL);
  if( ( newUrlAnchor !== oldUrlAnchor) && // if the url has changed
      ( lastAdded !== newUrlAnchor ) //  and it's not the page we just pushed.
   ){
    response = sanitizeHashChangeEvent(event.originalEvent);
  } else {
    response = false;
  }
  lastAdded = null;
  return response;
}

function sanitizeHashChangeEvent (e) {
  return {
    newURL: anchorFromUrl(e.newURL),
    oldURL: anchorFromUrl(e.oldURL)
  };
}

function callConnectHost(cb){
  if(require && require.amd){
    require(['connect-host'], function(connectHost){
      cb(connectHost);
    });
  } else {
    cb(window.connectHost);
  }
}

var hashChangeListenerRegistered = false;

function registerHashChangeListener(){
  if(hashChangeListenerRegistered) {
    return;
  }
  AJS.$(window).on("hashchange", function(e){
    var event = hashChange(e);
    if(event) {
      callConnectHost(function(ch){
        ch.broadcastEvent('history_popstate', {}, event);
      });
    }
  });
  hashChangeListenerRegistered = true;
}

function changeState (anchor, replace) {
    var currentUrlAnchor = anchorFromUrl(window.location.href),
    newUrl = window.location.href.replace(window.location.hash,""),
    newUrlAnchor = addPrefix(anchor);

    // If the url has changed.
    if(anchor !== currentUrlAnchor){
        lastAdded = stripPrefix(newUrlAnchor);
        // If it was replaceState or pushState?
        if(replace){
            window.location.replace("#" + newUrlAnchor);
        } else {
            window.location.assign("#" + newUrlAnchor);
        }
        return newUrlAnchor;
    }
}

export default {
  /**
  * Goes back one step in the joint session history. Will invoke the popState callback.
  * @noDemo
  * @example
  * AP.history.back(); // go back by 1 entry in the browser history.
  */
  back: function(callback){
    callback = arguments[arguments.length - 1];
    this.go(-1, callback);
  },
  /**
  * Goes back one step in the joint session history. Will invoke the popState callback.
  * @noDemo
  * @example
  * AP.history.forward(); // go forward by 1 entry in the browser history.
  */
  forward: function(callback){
    callback = arguments[arguments.length - 1];
    this.go(1, callback);
  },
  /**
  * Retrieves the current state of the history stack and returns the value. The returned value is the same as what was set with the pushState method.
  * @return {String} The current url anchor
  * @noDemo
  * @example
  * AP.history.pushState("page5");
  * AP.history.getState(); // returns "page5";
  */
  getState: function(callback){
    callback = arguments[arguments.length - 1];
    callback(stripPrefix(stripHash(window.location.hash)));
  },
  /**
  * Moves the page history back or forward the specified number of steps. A zero delta will reload the current page.
  * If the delta is out of range, does nothing. This call invoke the popState callback.
  * @param {Integer} delta - Number of places to move in the history
  * @noDemo
  * @example
  * AP.history.go(-2); // go back by 2 entries in the browser history.
  */
  go: function(delta){
    var callback = arguments[arguments.length - 1];
    if(callback._context.extension.options.isFullPage) {
        window.history.go(delta);
    } else {
        AJS.log("History is only available to page modules");
    }
  },
  _registerPopStateListener: function(){
      registerHashChangeListener();
  },
  /**
  * Updates the location's anchor with the specified value and pushes the given data onto the session history.
  * Does not invoke popState callback.
  * @param {String} url - URL to add to history
  */
  pushState: function(newState, callback){
    callback = arguments[arguments.length - 1];
    if(callback._context.extension.options.isFullPage) {
        changeState(newState);
    } else {
        AJS.log("History is only available to page modules");
    }
  },
  /**
  * Updates the current entry in the session history. Updates the location's anchor with the specified value but does not change the session history. Does not invoke popState callback.
  * @param {String} url - URL to update current history value with
  */
  replaceState: function(newState, callback) {
    callback = arguments[arguments.length - 1];
    if(callback._context.extension.options.isFullPage) {
        changeState(newState, true);
    } else {
        AJS.log("History is only available to page modules");
    }
  }

  /**
   * Register a function to be executed on state change.
   * @name popState
   * @method   
   * @param {Function} callback - Callback method to be executed on state change.
   */
};