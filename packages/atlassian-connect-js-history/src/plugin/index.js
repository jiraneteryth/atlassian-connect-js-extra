(function(){
  if(!AP._hostModules.history) {
    return;
  }

  var popStateCallbacks = [];
  function popState(callback) {
    AP.history._registerPopStateListener();
    popStateCallbacks.push(callback);
  }

  AP.register({
    history_popstate: function(data) {
      popStateCallbacks.forEach(function(cb){
        cb(data);
      });
    }
  });

  AP._hostModules.history.popState = popState;
  AP.history.popState = popState;


}());

