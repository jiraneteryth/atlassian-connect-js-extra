(function(f){if(typeof exports==="object"&&typeof module!=="undefined"){module.exports=f()}else if(typeof define==="function"&&define.amd){define([],f)}else{var g;if(typeof window!=="undefined"){g=window}else if(typeof global!=="undefined"){g=global}else if(typeof self!=="undefined"){g=self}else{g=this}g.connectHostRequest = f()}})(function(){var define,module,exports;return (function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(_dereq_,module,exports){
/**
* The Request Javascript module provides a mechanism for an add-on rendered in an iframe to make an XMLHttpRequest to the host product without requiring CORS.
*
* In contrast to REST calls made from the add-on server to the product directly, any requests made in the browser are evaluated in the context of the currently logged in user. The requested resource is still evaluated against the add-ons granted scopes.
*
* @name request
* @module
* @example
* AP.request('../assets/js/rest-example.json', {
*   success: function(responseText){
*     alert(responseText);
*   }
* });
*/

'use strict';

Object.defineProperty(exports, '__esModule', {
  value: true
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _dollar = _dereq_('./dollar');

var _dollar2 = _interopRequireDefault(_dollar);

var XHR_PROPERTIES = ['status', 'statusText', 'responseText'];
var XHR_HEADERS = ['Content-Type', 'ETag'];
var REQUEST_HEADERS_WHITELIST = ['If-Match', 'If-None-Match'];
var contextPath = null;

// reduce the xhr object to the just bits we can/want to expose over the bridge
function toJSON(xhr) {
  var json = { headers: {} };
  // only copy key properties and headers for transport across the bridge
  _dollar2['default'].each(XHR_PROPERTIES, function (i, v) {
    json[v] = xhr[v];
  });
  // only copy key response headers for transport across the bridge
  _dollar2['default'].each(XHR_HEADERS, function (i, v) {
    json.headers[v] = xhr.getResponseHeader(v);
  });
  return json;
}

function appendFormData(formData, key, value) {
  if (value._isBlob && value.blob && value.name) {
    formData.append(key, value.blob, value.name);
  } else {
    formData.append(key, value);
  }
  return formData;
}

function handleMultipartRequest(ajaxOptions) {
  ajaxOptions.contentType = false;
  ajaxOptions.processData = false;

  if (ajaxOptions.data && typeof ajaxOptions.data === 'object') {
    var formData = new FormData();
    Object.keys(ajaxOptions.data).forEach(function (key) {
      var formValue = ajaxOptions.data[key];
      if (Array.isArray(formValue)) {
        formValue.forEach(function (val, index) {
          formData = appendFormData(formData, key + '[' + index + ']', val);
        });
      } else {
        formData = appendFormData(formData, key, formValue);
      }
    });
    ajaxOptions.data = formData;
    ajaxOptions.headers['X-Atlassian-Token'] = 'no-check';
  } else {
    throw new Error('For a Multipart request, data must to be an Object');
  }
  return ajaxOptions;
}

exports['default'] = {
  /**
  * Execute an XMLHttpRequest in the context of the host application. The format of the response (dataType) will always be set to "text" - even if specified.
  *
  * @param {String}         url                           Either the URI to request or an options object (as below) containing at least a 'url' property;<br />
  *                                                       This value should be relative to the context path of the host application.
  * @param {Object}         options                       The options of the request.
  * @param {String}         options.url                   The url to request from the host application, relative to the host's context path
  * @param {String}         [options.type=GET]            The HTTP method name.
  * @param {Boolean}        [options.cache=true]          If the request should be cached.
  * @param {String|Object}  [options.data]                The body of the request; required if type is 'POST' or 'PUT'
  * @param {String}         [options.contentType]         The content-type string value of the entity body, above; required when data is supplied.
  * @param {Object}         [options.headers]             An object containing headers to set; supported headers are: 'Accept', 'If-Match' and 'If-None-Match'.
  * @param {Function}       [options.success]             A callback function executed on a 200 success status code.
  * @param {Function}       [options.error]               A callback function executed when a HTTP status error code is returned.
  * @param {Boolean}        [options.experimental=false]  If this is set to true, the developer acknowledges that the API endpoint which is being called may be in beta state, and thus may also have a shorter deprecation cycle than stable APIs.
  * @example
  * // A simple POST request which logs response in the console.
  * AP.request({
  *   url: '../assets/js/rest-example.json',
  *   type: 'POST',
  *   data: {name: 'some text', description: 'test'}
  *   success: function(responseText){
  *     console.log(responseText);
  *   },
  *   error: function(xhr, statusText, errorThrown){
  *     console.log(arguments);
  *   }
  * });
   * @example
  * // Upload an attachment to a Confluence entity.
  * var fileToUpload = document.getElementById("fileInput").files[0];
  *
  * AP.request({
  *   url: '/rest/api/content/123456/child/attachment',
  *   type: 'POST',
  *   contentType: 'multipart/form-data',
  *   data: {comment: 'example comment', file: fileToUpload},
  *   success: function(responseText){
  *     alert(responseText);
  *   }
  * });
  */
  request: function request(args, callback) {
    var url = AJS.contextPath();
    var headers = {};

    callback = arguments[arguments.length - 1];
    if (typeof args === 'string') {
      args = { url: args };
    }

    url += args.url;
    url = url.replace(/\/\.\.\//ig, ''); // strip /../ from urls

    _dollar2['default'].each(args.headers || {}, function (k, v) {
      headers[k.toLowerCase()] = v;
    });

    function done(data, textStatus, xhr) {
      callback(false, toJSON(xhr), data);
    }
    function fail(xhr, textStatus, errorThrown) {
      callback(errorThrown, toJSON(xhr), errorThrown);
    }

    // Disable system ajax settings. This stops confluence mobile from injecting callbacks and then throwing exceptions.
    // $.ajaxSettings = {};

    // execute the request with our restricted set of inputs
    var ajaxOptions = {
      url: url,
      type: args.type || 'GET',
      data: args.data,
      dataType: 'text', // prevent jquery from parsing the response body
      contentType: args.contentType,
      cache: typeof args.cache !== 'undefined' ? !!args.cache : undefined,
      headers: {
        // */* will undo the effect on the accept header of having set dataType to 'text'
        'Accept': headers.accept || '*/*',
        // send the client key header to force scope checks
        'AP-Client-Key': callback._context.extension.addon_key
      }
    };

    // if it's a multipart request, then transform data into a FormData object
    if (ajaxOptions.contentType === 'multipart/form-data') {
      ajaxOptions = handleMultipartRequest(ajaxOptions);
    }

    // set the experimental header
    if (args.experimental === true) {
      ajaxOptions.headers["X-ExperimentalApi"] = "opt-in";
    }

    _dollar2['default'].each(REQUEST_HEADERS_WHITELIST, function (index, header) {
      if (headers[header.toLowerCase()]) {
        ajaxOptions.headers[header] = headers[header.toLowerCase()];
      }
    });

    _dollar2['default'].ajax(ajaxOptions).then(done, fail);
  }
};
module.exports = exports['default'];

},{"./dollar":2}],2:[function(_dereq_,module,exports){
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = AJS.$;
module.exports = exports["default"];

},{}]},{},[1])(1)
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCIvVXNlcnMva2hhbmd1eWVuL3NyYy9hdGxhc3NpYW4tY29ubmVjdC1qcy1leHRyYS9wYWNrYWdlcy9hdGxhc3NpYW4tY29ubmVjdC1qcy1yZXF1ZXN0L3NyYy9ob3N0L2luZGV4LmpzIiwiL1VzZXJzL2toYW5ndXllbi9zcmMvYXRsYXNzaWFuLWNvbm5lY3QtanMtZXh0cmEvcGFja2FnZXMvYXRsYXNzaWFuLWNvbm5lY3QtanMtcmVxdWVzdC9zcmMvaG9zdC9kb2xsYXIuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztzQkNlYyxVQUFVOzs7O0FBRXhCLElBQU0sY0FBYyxHQUFHLENBQUMsUUFBUSxFQUFFLFlBQVksRUFBRSxjQUFjLENBQUMsQ0FBQztBQUNoRSxJQUFNLFdBQVcsR0FBRyxDQUFDLGNBQWMsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUM3QyxJQUFNLHlCQUF5QixHQUFHLENBQUMsVUFBVSxFQUFFLGVBQWUsQ0FBQyxDQUFDO0FBQ2hFLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQzs7O0FBR3ZCLFNBQVMsTUFBTSxDQUFDLEdBQUcsRUFBRTtBQUNuQixNQUFJLElBQUksR0FBRyxFQUFDLE9BQU8sRUFBRSxFQUFFLEVBQUMsQ0FBQzs7QUFFekIsc0JBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUMsRUFBRSxDQUFDLEVBQUU7QUFBRSxRQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO0dBQUUsQ0FBQyxDQUFDOztBQUU5RCxzQkFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxFQUFFLENBQUMsRUFBRTtBQUFFLFFBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxDQUFDO0dBQUUsQ0FBQyxDQUFDO0FBQ3JGLFNBQU8sSUFBSSxDQUFDO0NBQ2I7O0FBRUQsU0FBUyxjQUFjLENBQUMsUUFBUSxFQUFFLEdBQUcsRUFBRSxLQUFLLEVBQUU7QUFDNUMsTUFBSSxLQUFLLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksS0FBSyxDQUFDLElBQUksRUFBRTtBQUM3QyxZQUFRLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztHQUM5QyxNQUFNO0FBQ0wsWUFBUSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7R0FDN0I7QUFDRCxTQUFPLFFBQVEsQ0FBQztDQUNqQjs7QUFFRCxTQUFTLHNCQUFzQixDQUFFLFdBQVcsRUFBRTtBQUM1QyxhQUFXLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQztBQUNoQyxhQUFXLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQzs7QUFFaEMsTUFBSSxXQUFXLENBQUMsSUFBSSxJQUFJLE9BQU8sV0FBVyxDQUFDLElBQUksS0FBSyxRQUFRLEVBQUU7QUFDNUQsUUFBSSxRQUFRLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztBQUM5QixVQUFNLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsVUFBVSxHQUFHLEVBQUU7QUFDbkQsVUFBSSxTQUFTLEdBQUcsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztBQUN0QyxVQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLEVBQUU7QUFDNUIsaUJBQVMsQ0FBQyxPQUFPLENBQUMsVUFBVSxHQUFHLEVBQUUsS0FBSyxFQUFFO0FBQ3RDLGtCQUFRLEdBQUcsY0FBYyxDQUFDLFFBQVEsRUFBSyxHQUFHLFNBQUksS0FBSyxRQUFLLEdBQUcsQ0FBQyxDQUFDO1NBQzlELENBQUMsQ0FBQTtPQUNILE1BQU07QUFDTCxnQkFBUSxHQUFHLGNBQWMsQ0FBQyxRQUFRLEVBQUUsR0FBRyxFQUFFLFNBQVMsQ0FBQyxDQUFDO09BQ3JEO0tBQ0YsQ0FBQyxDQUFDO0FBQ0gsZUFBVyxDQUFDLElBQUksR0FBRyxRQUFRLENBQUM7QUFDNUIsZUFBVyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxHQUFHLFVBQVUsQ0FBQztHQUN2RCxNQUFNO0FBQ0wsVUFBTSxJQUFJLEtBQUssQ0FBQyxvREFBb0QsQ0FBQyxDQUFDO0dBQ3ZFO0FBQ0QsU0FBTyxXQUFXLENBQUM7Q0FDcEI7O3FCQUVjOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBNENiLFNBQU8sRUFBRSxpQkFBUyxJQUFJLEVBQUUsUUFBUSxFQUFDO0FBQy9CLFFBQUksR0FBRyxHQUFHLEdBQUcsQ0FBQyxXQUFXLEVBQUUsQ0FBQztBQUM1QixRQUFJLE9BQU8sR0FBRyxFQUFFLENBQUM7O0FBRWpCLFlBQVEsR0FBRyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztBQUMzQyxRQUFHLE9BQU8sSUFBSSxLQUFLLFFBQVEsRUFBRTtBQUMzQixVQUFJLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFDLENBQUM7S0FDckI7O0FBRUQsT0FBRyxJQUFJLElBQUksQ0FBQyxHQUFHLENBQUM7QUFDaEIsT0FBRyxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsWUFBWSxFQUFDLEVBQUUsQ0FBQyxDQUFDOztBQUVuQyx3QkFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxFQUFFLEVBQUUsVUFBVSxDQUFDLEVBQUUsQ0FBQyxFQUFFO0FBQ3pDLGFBQU8sQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7S0FDOUIsQ0FBQyxDQUFDOztBQUVILGFBQVMsSUFBSSxDQUFDLElBQUksRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFO0FBQ25DLGNBQVEsQ0FBQyxLQUFLLEVBQUUsTUFBTSxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDO0tBQ3BDO0FBQ0QsYUFBUyxJQUFJLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUU7QUFDMUMsY0FBUSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsR0FBRyxDQUFDLEVBQUUsV0FBVyxDQUFDLENBQUM7S0FDakQ7Ozs7OztBQU1ELFFBQUksV0FBVyxHQUFHO0FBQ2hCLFNBQUcsRUFBRSxHQUFHO0FBQ1IsVUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLElBQUksS0FBSztBQUN4QixVQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7QUFDZixjQUFRLEVBQUUsTUFBTTtBQUNoQixpQkFBVyxFQUFFLElBQUksQ0FBQyxXQUFXO0FBQzdCLFdBQUssRUFBRSxBQUFDLE9BQU8sSUFBSSxDQUFDLEtBQUssS0FBSyxXQUFXLEdBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsU0FBUztBQUNyRSxhQUFPLEVBQUU7O0FBRVAsZ0JBQVEsRUFBRSxPQUFPLENBQUMsTUFBTSxJQUFJLEtBQUs7O0FBRWpDLHVCQUFlLEVBQUUsUUFBUSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsU0FBUztPQUN2RDtLQUNGLENBQUM7OztBQUdGLFFBQUksV0FBVyxDQUFDLFdBQVcsS0FBSyxxQkFBcUIsRUFBRTtBQUNyRCxpQkFBVyxHQUFHLHNCQUFzQixDQUFDLFdBQVcsQ0FBQyxDQUFDO0tBQ25EOzs7QUFHRCxRQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssSUFBSSxFQUFFO0FBQzlCLGlCQUFXLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLEdBQUcsUUFBUSxDQUFDO0tBQ3JEOztBQUVELHdCQUFFLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxVQUFVLEtBQUssRUFBRSxNQUFNLEVBQUU7QUFDekQsVUFBSSxPQUFPLENBQUMsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDLEVBQUU7QUFDakMsbUJBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDO09BQzdEO0tBQ0YsQ0FBQyxDQUFDOztBQUVILHdCQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO0dBQ3RDO0NBQ0Y7Ozs7Ozs7OztxQkN6S2MsR0FBRyxDQUFDLENBQUMiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiLyoqXG4qIFRoZSBSZXF1ZXN0IEphdmFzY3JpcHQgbW9kdWxlIHByb3ZpZGVzIGEgbWVjaGFuaXNtIGZvciBhbiBhZGQtb24gcmVuZGVyZWQgaW4gYW4gaWZyYW1lIHRvIG1ha2UgYW4gWE1MSHR0cFJlcXVlc3QgdG8gdGhlIGhvc3QgcHJvZHVjdCB3aXRob3V0IHJlcXVpcmluZyBDT1JTLlxuKlxuKiBJbiBjb250cmFzdCB0byBSRVNUIGNhbGxzIG1hZGUgZnJvbSB0aGUgYWRkLW9uIHNlcnZlciB0byB0aGUgcHJvZHVjdCBkaXJlY3RseSwgYW55IHJlcXVlc3RzIG1hZGUgaW4gdGhlIGJyb3dzZXIgYXJlIGV2YWx1YXRlZCBpbiB0aGUgY29udGV4dCBvZiB0aGUgY3VycmVudGx5IGxvZ2dlZCBpbiB1c2VyLiBUaGUgcmVxdWVzdGVkIHJlc291cmNlIGlzIHN0aWxsIGV2YWx1YXRlZCBhZ2FpbnN0IHRoZSBhZGQtb25zIGdyYW50ZWQgc2NvcGVzLlxuKlxuKiBAbmFtZSByZXF1ZXN0XG4qIEBtb2R1bGVcbiogQGV4YW1wbGVcbiogQVAucmVxdWVzdCgnLi4vYXNzZXRzL2pzL3Jlc3QtZXhhbXBsZS5qc29uJywge1xuKiAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlVGV4dCl7XG4qICAgICBhbGVydChyZXNwb25zZVRleHQpO1xuKiAgIH1cbiogfSk7XG4qL1xuXG5pbXBvcnQgJCBmcm9tICcuL2RvbGxhcic7XG5cbmNvbnN0IFhIUl9QUk9QRVJUSUVTID0gWydzdGF0dXMnLCAnc3RhdHVzVGV4dCcsICdyZXNwb25zZVRleHQnXTtcbmNvbnN0IFhIUl9IRUFERVJTID0gWydDb250ZW50LVR5cGUnLCAnRVRhZyddO1xuY29uc3QgUkVRVUVTVF9IRUFERVJTX1dISVRFTElTVCA9IFsnSWYtTWF0Y2gnLCAnSWYtTm9uZS1NYXRjaCddO1xudmFyIGNvbnRleHRQYXRoID0gbnVsbDtcblxuLy8gcmVkdWNlIHRoZSB4aHIgb2JqZWN0IHRvIHRoZSBqdXN0IGJpdHMgd2UgY2FuL3dhbnQgdG8gZXhwb3NlIG92ZXIgdGhlIGJyaWRnZVxuZnVuY3Rpb24gdG9KU09OKHhocikge1xuICB2YXIganNvbiA9IHtoZWFkZXJzOiB7fX07XG4gIC8vIG9ubHkgY29weSBrZXkgcHJvcGVydGllcyBhbmQgaGVhZGVycyBmb3IgdHJhbnNwb3J0IGFjcm9zcyB0aGUgYnJpZGdlXG4gICQuZWFjaChYSFJfUFJPUEVSVElFUywgZnVuY3Rpb24gKGksIHYpIHsganNvblt2XSA9IHhoclt2XTsgfSk7XG4gIC8vIG9ubHkgY29weSBrZXkgcmVzcG9uc2UgaGVhZGVycyBmb3IgdHJhbnNwb3J0IGFjcm9zcyB0aGUgYnJpZGdlXG4gICQuZWFjaChYSFJfSEVBREVSUywgZnVuY3Rpb24gKGksIHYpIHsganNvbi5oZWFkZXJzW3ZdID0geGhyLmdldFJlc3BvbnNlSGVhZGVyKHYpOyB9KTtcbiAgcmV0dXJuIGpzb247XG59XG5cbmZ1bmN0aW9uIGFwcGVuZEZvcm1EYXRhKGZvcm1EYXRhLCBrZXksIHZhbHVlKSB7XG4gIGlmICh2YWx1ZS5faXNCbG9iICYmIHZhbHVlLmJsb2IgJiYgdmFsdWUubmFtZSkge1xuICAgIGZvcm1EYXRhLmFwcGVuZChrZXksIHZhbHVlLmJsb2IsIHZhbHVlLm5hbWUpO1xuICB9IGVsc2Uge1xuICAgIGZvcm1EYXRhLmFwcGVuZChrZXksIHZhbHVlKTtcbiAgfVxuICByZXR1cm4gZm9ybURhdGE7XG59XG5cbmZ1bmN0aW9uIGhhbmRsZU11bHRpcGFydFJlcXVlc3QgKGFqYXhPcHRpb25zKSB7XG4gIGFqYXhPcHRpb25zLmNvbnRlbnRUeXBlID0gZmFsc2U7XG4gIGFqYXhPcHRpb25zLnByb2Nlc3NEYXRhID0gZmFsc2U7XG5cbiAgaWYgKGFqYXhPcHRpb25zLmRhdGEgJiYgdHlwZW9mIGFqYXhPcHRpb25zLmRhdGEgPT09ICdvYmplY3QnKSB7XG4gICAgdmFyIGZvcm1EYXRhID0gbmV3IEZvcm1EYXRhKCk7XG4gICAgT2JqZWN0LmtleXMoYWpheE9wdGlvbnMuZGF0YSkuZm9yRWFjaChmdW5jdGlvbiAoa2V5KSB7XG4gICAgICB2YXIgZm9ybVZhbHVlID0gYWpheE9wdGlvbnMuZGF0YVtrZXldOyAgICAgIFxuICAgICAgaWYgKEFycmF5LmlzQXJyYXkoZm9ybVZhbHVlKSkge1xuICAgICAgICBmb3JtVmFsdWUuZm9yRWFjaChmdW5jdGlvbiAodmFsLCBpbmRleCkge1xuICAgICAgICAgIGZvcm1EYXRhID0gYXBwZW5kRm9ybURhdGEoZm9ybURhdGEsIGAke2tleX1bJHtpbmRleH1dYCwgdmFsKTtcbiAgICAgICAgfSlcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGZvcm1EYXRhID0gYXBwZW5kRm9ybURhdGEoZm9ybURhdGEsIGtleSwgZm9ybVZhbHVlKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICBhamF4T3B0aW9ucy5kYXRhID0gZm9ybURhdGE7XG4gICAgYWpheE9wdGlvbnMuaGVhZGVyc1snWC1BdGxhc3NpYW4tVG9rZW4nXSA9ICduby1jaGVjayc7XG4gIH0gZWxzZSB7XG4gICAgdGhyb3cgbmV3IEVycm9yKCdGb3IgYSBNdWx0aXBhcnQgcmVxdWVzdCwgZGF0YSBtdXN0IHRvIGJlIGFuIE9iamVjdCcpO1xuICB9XG4gIHJldHVybiBhamF4T3B0aW9ucztcbn1cblxuZXhwb3J0IGRlZmF1bHQge1xuICAvKipcbiAgKiBFeGVjdXRlIGFuIFhNTEh0dHBSZXF1ZXN0IGluIHRoZSBjb250ZXh0IG9mIHRoZSBob3N0IGFwcGxpY2F0aW9uLiBUaGUgZm9ybWF0IG9mIHRoZSByZXNwb25zZSAoZGF0YVR5cGUpIHdpbGwgYWx3YXlzIGJlIHNldCB0byBcInRleHRcIiAtIGV2ZW4gaWYgc3BlY2lmaWVkLlxuICAqXG4gICogQHBhcmFtIHtTdHJpbmd9ICAgICAgICAgdXJsICAgICAgICAgICAgICAgICAgICAgICAgICAgRWl0aGVyIHRoZSBVUkkgdG8gcmVxdWVzdCBvciBhbiBvcHRpb25zIG9iamVjdCAoYXMgYmVsb3cpIGNvbnRhaW5pbmcgYXQgbGVhc3QgYSAndXJsJyBwcm9wZXJ0eTs8YnIgLz5cbiAgKiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBUaGlzIHZhbHVlIHNob3VsZCBiZSByZWxhdGl2ZSB0byB0aGUgY29udGV4dCBwYXRoIG9mIHRoZSBob3N0IGFwcGxpY2F0aW9uLlxuICAqIEBwYXJhbSB7T2JqZWN0fSAgICAgICAgIG9wdGlvbnMgICAgICAgICAgICAgICAgICAgICAgIFRoZSBvcHRpb25zIG9mIHRoZSByZXF1ZXN0LlxuICAqIEBwYXJhbSB7U3RyaW5nfSAgICAgICAgIG9wdGlvbnMudXJsICAgICAgICAgICAgICAgICAgIFRoZSB1cmwgdG8gcmVxdWVzdCBmcm9tIHRoZSBob3N0IGFwcGxpY2F0aW9uLCByZWxhdGl2ZSB0byB0aGUgaG9zdCdzIGNvbnRleHQgcGF0aFxuICAqIEBwYXJhbSB7U3RyaW5nfSAgICAgICAgIFtvcHRpb25zLnR5cGU9R0VUXSAgICAgICAgICAgIFRoZSBIVFRQIG1ldGhvZCBuYW1lLlxuICAqIEBwYXJhbSB7Qm9vbGVhbn0gICAgICAgIFtvcHRpb25zLmNhY2hlPXRydWVdICAgICAgICAgIElmIHRoZSByZXF1ZXN0IHNob3VsZCBiZSBjYWNoZWQuXG4gICogQHBhcmFtIHtTdHJpbmd8T2JqZWN0fSAgW29wdGlvbnMuZGF0YV0gICAgICAgICAgICAgICAgVGhlIGJvZHkgb2YgdGhlIHJlcXVlc3Q7IHJlcXVpcmVkIGlmIHR5cGUgaXMgJ1BPU1QnIG9yICdQVVQnXG4gICogQHBhcmFtIHtTdHJpbmd9ICAgICAgICAgW29wdGlvbnMuY29udGVudFR5cGVdICAgICAgICAgVGhlIGNvbnRlbnQtdHlwZSBzdHJpbmcgdmFsdWUgb2YgdGhlIGVudGl0eSBib2R5LCBhYm92ZTsgcmVxdWlyZWQgd2hlbiBkYXRhIGlzIHN1cHBsaWVkLlxuICAqIEBwYXJhbSB7T2JqZWN0fSAgICAgICAgIFtvcHRpb25zLmhlYWRlcnNdICAgICAgICAgICAgIEFuIG9iamVjdCBjb250YWluaW5nIGhlYWRlcnMgdG8gc2V0OyBzdXBwb3J0ZWQgaGVhZGVycyBhcmU6ICdBY2NlcHQnLCAnSWYtTWF0Y2gnIGFuZCAnSWYtTm9uZS1NYXRjaCcuXG4gICogQHBhcmFtIHtGdW5jdGlvbn0gICAgICAgW29wdGlvbnMuc3VjY2Vzc10gICAgICAgICAgICAgQSBjYWxsYmFjayBmdW5jdGlvbiBleGVjdXRlZCBvbiBhIDIwMCBzdWNjZXNzIHN0YXR1cyBjb2RlLlxuICAqIEBwYXJhbSB7RnVuY3Rpb259ICAgICAgIFtvcHRpb25zLmVycm9yXSAgICAgICAgICAgICAgIEEgY2FsbGJhY2sgZnVuY3Rpb24gZXhlY3V0ZWQgd2hlbiBhIEhUVFAgc3RhdHVzIGVycm9yIGNvZGUgaXMgcmV0dXJuZWQuXG4gICogQHBhcmFtIHtCb29sZWFufSAgICAgICAgW29wdGlvbnMuZXhwZXJpbWVudGFsPWZhbHNlXSAgSWYgdGhpcyBpcyBzZXQgdG8gdHJ1ZSwgdGhlIGRldmVsb3BlciBhY2tub3dsZWRnZXMgdGhhdCB0aGUgQVBJIGVuZHBvaW50IHdoaWNoIGlzIGJlaW5nIGNhbGxlZCBtYXkgYmUgaW4gYmV0YSBzdGF0ZSwgYW5kIHRodXMgbWF5IGFsc28gaGF2ZSBhIHNob3J0ZXIgZGVwcmVjYXRpb24gY3ljbGUgdGhhbiBzdGFibGUgQVBJcy5cbiAgKiBAZXhhbXBsZVxuICAqIC8vIEEgc2ltcGxlIFBPU1QgcmVxdWVzdCB3aGljaCBsb2dzIHJlc3BvbnNlIGluIHRoZSBjb25zb2xlLlxuICAqIEFQLnJlcXVlc3Qoe1xuICAqICAgdXJsOiAnLi4vYXNzZXRzL2pzL3Jlc3QtZXhhbXBsZS5qc29uJyxcbiAgKiAgIHR5cGU6ICdQT1NUJyxcbiAgKiAgIGRhdGE6IHtuYW1lOiAnc29tZSB0ZXh0JywgZGVzY3JpcHRpb246ICd0ZXN0J31cbiAgKiAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlVGV4dCl7XG4gICogICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlVGV4dCk7XG4gICogICB9LFxuICAqICAgZXJyb3I6IGZ1bmN0aW9uKHhociwgc3RhdHVzVGV4dCwgZXJyb3JUaHJvd24pe1xuICAqICAgICBjb25zb2xlLmxvZyhhcmd1bWVudHMpO1xuICAqICAgfVxuICAqIH0pO1xuXG4gICogQGV4YW1wbGVcbiAgKiAvLyBVcGxvYWQgYW4gYXR0YWNobWVudCB0byBhIENvbmZsdWVuY2UgZW50aXR5LlxuICAqIHZhciBmaWxlVG9VcGxvYWQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImZpbGVJbnB1dFwiKS5maWxlc1swXTtcbiAgKlxuICAqIEFQLnJlcXVlc3Qoe1xuICAqICAgdXJsOiAnL3Jlc3QvYXBpL2NvbnRlbnQvMTIzNDU2L2NoaWxkL2F0dGFjaG1lbnQnLFxuICAqICAgdHlwZTogJ1BPU1QnLFxuICAqICAgY29udGVudFR5cGU6ICdtdWx0aXBhcnQvZm9ybS1kYXRhJyxcbiAgKiAgIGRhdGE6IHtjb21tZW50OiAnZXhhbXBsZSBjb21tZW50JywgZmlsZTogZmlsZVRvVXBsb2FkfSxcbiAgKiAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3BvbnNlVGV4dCl7XG4gICogICAgIGFsZXJ0KHJlc3BvbnNlVGV4dCk7XG4gICogICB9XG4gICogfSk7XG4gICovXG4gIHJlcXVlc3Q6IGZ1bmN0aW9uKGFyZ3MsIGNhbGxiYWNrKXtcbiAgICB2YXIgdXJsID0gQUpTLmNvbnRleHRQYXRoKCk7XG4gICAgdmFyIGhlYWRlcnMgPSB7fTtcblxuICAgIGNhbGxiYWNrID0gYXJndW1lbnRzW2FyZ3VtZW50cy5sZW5ndGggLSAxXTtcbiAgICBpZih0eXBlb2YgYXJncyA9PT0gJ3N0cmluZycpIHtcbiAgICAgIGFyZ3MgPSB7IHVybDogYXJnc307XG4gICAgfVxuXG4gICAgdXJsICs9IGFyZ3MudXJsO1xuICAgIHVybCA9IHVybC5yZXBsYWNlKC9cXC9cXC5cXC5cXC8vaWcsJycpOyAvLyBzdHJpcCAvLi4vIGZyb20gdXJsc1xuXG4gICAgJC5lYWNoKGFyZ3MuaGVhZGVycyB8fCB7fSwgZnVuY3Rpb24gKGssIHYpIHtcbiAgICAgIGhlYWRlcnNbay50b0xvd2VyQ2FzZSgpXSA9IHY7XG4gICAgfSk7XG5cbiAgICBmdW5jdGlvbiBkb25lKGRhdGEsIHRleHRTdGF0dXMsIHhocikge1xuICAgICAgY2FsbGJhY2soZmFsc2UsIHRvSlNPTih4aHIpLCBkYXRhKTtcbiAgICB9XG4gICAgZnVuY3Rpb24gZmFpbCh4aHIsIHRleHRTdGF0dXMsIGVycm9yVGhyb3duKSB7XG4gICAgICBjYWxsYmFjayhlcnJvclRocm93biwgdG9KU09OKHhociksIGVycm9yVGhyb3duKTtcbiAgICB9XG5cbiAgICAvLyBEaXNhYmxlIHN5c3RlbSBhamF4IHNldHRpbmdzLiBUaGlzIHN0b3BzIGNvbmZsdWVuY2UgbW9iaWxlIGZyb20gaW5qZWN0aW5nIGNhbGxiYWNrcyBhbmQgdGhlbiB0aHJvd2luZyBleGNlcHRpb25zLlxuICAgIC8vICQuYWpheFNldHRpbmdzID0ge307XG5cbiAgICAvLyBleGVjdXRlIHRoZSByZXF1ZXN0IHdpdGggb3VyIHJlc3RyaWN0ZWQgc2V0IG9mIGlucHV0c1xuICAgIHZhciBhamF4T3B0aW9ucyA9IHtcbiAgICAgIHVybDogdXJsLFxuICAgICAgdHlwZTogYXJncy50eXBlIHx8ICdHRVQnLFxuICAgICAgZGF0YTogYXJncy5kYXRhLFxuICAgICAgZGF0YVR5cGU6ICd0ZXh0JywgLy8gcHJldmVudCBqcXVlcnkgZnJvbSBwYXJzaW5nIHRoZSByZXNwb25zZSBib2R5XG4gICAgICBjb250ZW50VHlwZTogYXJncy5jb250ZW50VHlwZSxcbiAgICAgIGNhY2hlOiAodHlwZW9mIGFyZ3MuY2FjaGUgIT09ICd1bmRlZmluZWQnKSA/ICEhYXJncy5jYWNoZSA6IHVuZGVmaW5lZCxcbiAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgLy8gKi8qIHdpbGwgdW5kbyB0aGUgZWZmZWN0IG9uIHRoZSBhY2NlcHQgaGVhZGVyIG9mIGhhdmluZyBzZXQgZGF0YVR5cGUgdG8gJ3RleHQnXG4gICAgICAgICdBY2NlcHQnOiBoZWFkZXJzLmFjY2VwdCB8fCAnKi8qJyxcbiAgICAgICAgLy8gc2VuZCB0aGUgY2xpZW50IGtleSBoZWFkZXIgdG8gZm9yY2Ugc2NvcGUgY2hlY2tzXG4gICAgICAgICdBUC1DbGllbnQtS2V5JzogY2FsbGJhY2suX2NvbnRleHQuZXh0ZW5zaW9uLmFkZG9uX2tleVxuICAgICAgfVxuICAgIH07XG5cbiAgICAvLyBpZiBpdCdzIGEgbXVsdGlwYXJ0IHJlcXVlc3QsIHRoZW4gdHJhbnNmb3JtIGRhdGEgaW50byBhIEZvcm1EYXRhIG9iamVjdFxuICAgIGlmIChhamF4T3B0aW9ucy5jb250ZW50VHlwZSA9PT0gJ211bHRpcGFydC9mb3JtLWRhdGEnKSB7XG4gICAgICBhamF4T3B0aW9ucyA9IGhhbmRsZU11bHRpcGFydFJlcXVlc3QoYWpheE9wdGlvbnMpO1xuICAgIH1cblxuICAgIC8vIHNldCB0aGUgZXhwZXJpbWVudGFsIGhlYWRlclxuICAgIGlmIChhcmdzLmV4cGVyaW1lbnRhbCA9PT0gdHJ1ZSkge1xuICAgICAgYWpheE9wdGlvbnMuaGVhZGVyc1tcIlgtRXhwZXJpbWVudGFsQXBpXCJdID0gXCJvcHQtaW5cIjtcbiAgICB9XG5cbiAgICAkLmVhY2goUkVRVUVTVF9IRUFERVJTX1dISVRFTElTVCwgZnVuY3Rpb24gKGluZGV4LCBoZWFkZXIpIHtcbiAgICAgIGlmIChoZWFkZXJzW2hlYWRlci50b0xvd2VyQ2FzZSgpXSkge1xuICAgICAgICBhamF4T3B0aW9ucy5oZWFkZXJzW2hlYWRlcl0gPSBoZWFkZXJzW2hlYWRlci50b0xvd2VyQ2FzZSgpXTtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgICQuYWpheChhamF4T3B0aW9ucykudGhlbihkb25lLCBmYWlsKTtcbiAgfVxufTtcbiIsImV4cG9ydCBkZWZhdWx0IEFKUy4kOyJdfQ==
