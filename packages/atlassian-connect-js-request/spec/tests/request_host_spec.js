import requestHost from '../../src/host';

describe('request', () => {

  window.AJS.contextPath = () => '';

  const defaultContext = {
    extension: {
      addon_key: 'my-add-on'
    }
  }

  it('calls $.ajax with correct options', () => {
    const requestOptions = {
      url: '/some/url'
    }
    const callback = jasmine.createSpy('callback');
    callback._context = defaultContext;

    spyOn($, 'ajax').and.callFake((options) => {
      expect(options.url).toEqual(requestOptions.url);
      expect(options.type).toEqual('GET');
      expect(options.data).not.toBeDefined();
      expect(options.headers['Accept']).toEqual('*/*');
      expect(options.headers['AP-Client-Key']).toEqual(defaultContext.extension.addon_key);      
      return $.Deferred((defer) => {
        defer.resolve('data', 'statusText', new XMLHttpRequest());
      });
    });

    requestHost.request(requestOptions, callback);
    expect($.ajax).toHaveBeenCalled()
  });

  it('headers are properly whitelisted', () => {
    const ifMatchVal = 'if-match-value';
    const ifNoneMatchVal = 'if-none-match-value';
    const requestOptions = {
      url: '/some/url',
      headers: {
        'If-Match': ifMatchVal,
        'If-None-Match': ifNoneMatchVal,
        'Not-Valid-Header': 'does not matter'
      }
    }
    const callback = jasmine.createSpy('callback');
    callback._context = defaultContext;

    spyOn($, 'ajax').and.callFake((options) => {
      expect(options.headers['If-Match']).toEqual(ifMatchVal);
      expect(options.headers['If-None-Match']).toEqual(ifNoneMatchVal);
      expect(options.headers['Not-Valid-Header']).not.toBeDefined();
      return $.Deferred((defer) => {
        defer.resolve('data', 'statusText', new XMLHttpRequest());
      });
    });

    requestHost.request(requestOptions, callback);
    expect($.ajax).toHaveBeenCalled();
  });

  describe('experimental header', () => {
    it('is not set by default', () => {
      const requestOptions = {
        url: '/some/url'
      }
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;

      spyOn($, 'ajax').and.callFake((options) => {
        expect(options.headers['X-ExperimentalApi']).not.toBeDefined();
        return $.Deferred((defer) => {
          defer.resolve('data', 'statusText', new XMLHttpRequest());
        });
      });

      requestHost.request(requestOptions, callback);
      expect($.ajax).toHaveBeenCalled()
    });

    it('is set when experimetal is true', () => {
      const requestOptions = {
        url: '/some/url',
        experimental: true
      }
      const callback = jasmine.createSpy('callback');
      callback._context = defaultContext;

      spyOn($, 'ajax').and.callFake((options) => {
        expect(options.headers['X-ExperimentalApi']).toEqual('opt-in');
        return $.Deferred((defer) => {
          defer.resolve('data', 'statusText', new XMLHttpRequest());
        });
      });

      requestHost.request(requestOptions, callback);
      expect($.ajax).toHaveBeenCalled()
    });
  });


  describe('file upload', () => {
    const callback = jasmine.createSpy('callback');
    callback._context = defaultContext;
    const file = new Blob(['some file for upload']);
    file.name = 'somename.txt';
    const comment = 'this is a comment';
    const requestOptions = {
      url: '/some/url',
      type: 'POST',
      contentType: 'multipart/form-data',
      data: {
        file: file,
        comment: comment
      }
    };

    it('adds the X-Atlassian-Token header to the request', () => {
      spyOn($, 'ajax').and.callFake((options) => {
        expect(options.headers['X-Atlassian-Token']).toEqual('no-check');
        return $.Deferred((defer) => {
          defer.resolve('data', 'statusText', new XMLHttpRequest());
        });
      });
      requestHost.request(requestOptions, callback);
    });

    it('constructs the FormData', () => {
      spyOn($, 'ajax').and.callFake((options) => {
        expect(options.data instanceof FormData).toBe(true);
        return $.Deferred((defer) => {
          defer.resolve('data', 'statusText', new XMLHttpRequest());
        });
      });
      requestHost.request(requestOptions, callback);
    });

    it('throws an error when data is not an object', () => {
      const badrequestOptions = {
        url: '/some/url',
        type: 'POST',
        contentType: 'multipart/form-data',
        data: 'test'
      };
      try {
        requestHost.request(badrequestOptions, callback);
      } catch (e) {
        expect(e.message).toEqual('For a Multipart request, data must to be an Object');
      }
    });
  });

  describe('callbacks', () => {
    const requestOptions = {
      url: '/some/url'
    };

    it('done callback is called (JSON)', () => {
      const data = 'some data';
      const statusText = '200';
      const xhr = new XMLHttpRequest();

      const callback = jasmine.createSpy('callback').and.callFake((errorThrown, textStatus, cbdata) => {
        expect(errorThrown).toBe(false);
        expect(cbdata).toEqual(data);
        expect(textStatus).toBeDefined();
      });

      callback._context = defaultContext;

      spyOn($, 'ajax').and.callFake((options) => {
        return $.Deferred((defer) => {
          defer.resolve(data, statusText, xhr);
        });
      });
      requestHost.request(requestOptions, callback);
      expect(callback).toHaveBeenCalled();
    });

    it('fail callback is called (JSON)', () => {
      const error = 'some error';
      const statusText = '500';
      const xhr = new XMLHttpRequest();

      const callback = jasmine.createSpy('callback').and.callFake((errorThrown, textStatus, error2) => {
        expect(errorThrown).toEqual(error);
        expect(textStatus).toBeDefined();
        expect(error2).toEqual(error);
      });

      callback._context = defaultContext;

      spyOn($, 'ajax').and.callFake((options) => {
        return $.Deferred((defer) => {
          defer.reject(xhr, statusText, 'some error');
        });
      });
      requestHost.request(requestOptions, callback);
      expect(callback).toHaveBeenCalled();
    });
  });
});