describe('request plugin', () => {
  const requestStub = jasmine.createSpy('request');
  const AP = {
    _hostModules: {
      _globals: {
        request: requestStub
      }
    },
    request: requestStub
  }

  window.AP = AP;

  require('../../src/plugin/index');

  beforeEach(() => {
    window.AP = AP;
    requestStub.calls.reset();
  });

  it('overwrites the AP request modules', () => {
    expect(AP._hostModules._globals.request).not.toEqual(requestStub);
    expect(AP.request).not.toEqual(requestStub);
  });

  it('calls the original request method', () => {
    expect(requestStub).not.toHaveBeenCalled();
    let options = {
      url: 'some/url'
    };
    AP.request(options);
    expect(requestStub).toHaveBeenCalled();
  });

  it('calls the original request method', () => {
    let options = {
      url: 'some/url'
    }
    AP._hostModules._globals.request(options);
    expect(requestStub).toHaveBeenCalled();
  });

  describe('options handlers', () => {
    it('url, options', () => {
      let url = 'this/is/a/url';
      let data = {
        some: 'data'
      };
      let type = 'POST';
      let options = {
        type: type,
        data: data
      };

      AP.request(url, options);
      expect(requestStub).toHaveBeenCalled();
      let requestStubOptions = requestStub.calls.first().args[0];
      expect(requestStubOptions.url).toEqual(url);
      expect(requestStubOptions.type).toEqual(type);
      expect(requestStubOptions.data).toEqual(data);
    });

    it('options', () => {
      let options = {
        url: 'this/is/a/url',
        type: 'PUT',
        data: {
          some: 'data'
        }
      };
      AP.request(options);
      expect(requestStub).toHaveBeenCalled();
      expect(requestStub.calls.first().args[0]).toEqual(options);
    });
  });


  describe('callbacks', () => {

    const successSpy = jasmine.createSpy('success')
    const errorSpy = jasmine.createSpy('error')

    let options;

    beforeEach(() => {
      successSpy.calls.reset();
      errorSpy.calls.reset();
      requestStub.calls.reset();
      options = {
        url: 'some/url',
        success: successSpy,
        error: errorSpy
      }
    })

    it('are removed from request options', () => {
      expect(options.success).toBeDefined();
      expect(options.error).toBeDefined();
      AP.request(options);
      expect(requestStub.calls.first().args[0].success).not.toBeDefined();
      expect(requestStub.calls.first().args[0].error).not.toBeDefined();
    });

    it('success is called', () => {
      AP.request(options);
      expect(requestStub).toHaveBeenCalled();

      const callback = requestStub.calls.first().args[1];
      const testReponse = 'some response';
      const testBody = 'some test body';

      callback(false, testReponse, testBody);
      expect(successSpy).toHaveBeenCalled();
      expect(successSpy.calls.first().args[0]).toEqual(testBody);
      expect(successSpy.calls.first().args[1]).toEqual('success');
    });

    it('error is called', () => {
      AP.request(options);
      expect(requestStub).toHaveBeenCalled();

      const callback = requestStub.calls.first().args[1];
      const testReponse = 'some response';
      const testError = 'some test body';

      callback(testError, testReponse, testError);
      expect(errorSpy).toHaveBeenCalled();
      expect(errorSpy.calls.first().args[1]).toEqual('error');
      expect(errorSpy.calls.first().args[2]).toEqual(testError);
    });

  });

  describe('file upload', () => {
    beforeEach(() => {
      requestStub.calls.reset();
    });

    it('handles blob wrapping', () => {
      const testFilename = 'somefile.name';
      const testFile = new Blob(['test']);
      testFile.name = testFilename;
      const testString = 'some test string for request';

      const options = {
        url: 'some/url',
        type: 'POST',
        contentType: 'multipart/form-data',
        data: {
          file: testFile,
          fileInArray: [testFile],
          string: testString
        }
      };

      AP.request(options);

      var calledOptions = requestStub.calls.first().args[0];
      expect(calledOptions.data.file.blob).toEqual(testFile);
      expect(calledOptions.data.file.name).toEqual(testFilename);
      expect(calledOptions.data.file._isBlob).toEqual(true);
      expect(calledOptions.data.string).toEqual(testString);
      expect(calledOptions.data.fileInArray[0].blob).toEqual(testFile);
      expect(calledOptions.data.fileInArray[0].name).toEqual(testFilename);
      expect(calledOptions.data.fileInArray[0]._isBlob).toEqual(true);
    });
  });
});