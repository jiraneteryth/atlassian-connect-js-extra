/**
* The Request Javascript module provides a mechanism for an add-on rendered in an iframe to make an XMLHttpRequest to the host product without requiring CORS.
*
* In contrast to REST calls made from the add-on server to the product directly, any requests made in the browser are evaluated in the context of the currently logged in user. The requested resource is still evaluated against the add-ons granted scopes.
*
* @name request
* @module
* @example
* AP.request('../assets/js/rest-example.json', {
*   success: function(responseText){
*     alert(responseText);
*   }
* });
*/

import $ from './dollar';

const XHR_PROPERTIES = ['status', 'statusText', 'responseText'];
const XHR_HEADERS = ['Content-Type', 'ETag'];
const REQUEST_HEADERS_WHITELIST = ['If-Match', 'If-None-Match'];
var contextPath = null;

// reduce the xhr object to the just bits we can/want to expose over the bridge
function toJSON(xhr) {
  var json = {headers: {}};
  // only copy key properties and headers for transport across the bridge
  $.each(XHR_PROPERTIES, function (i, v) { json[v] = xhr[v]; });
  // only copy key response headers for transport across the bridge
  $.each(XHR_HEADERS, function (i, v) { json.headers[v] = xhr.getResponseHeader(v); });
  return json;
}

function appendFormData(formData, key, value) {
  if (value._isBlob && value.blob && value.name) {
    formData.append(key, value.blob, value.name);
  } else {
    formData.append(key, value);
  }
  return formData;
}

function handleMultipartRequest (ajaxOptions) {
  ajaxOptions.contentType = false;
  ajaxOptions.processData = false;

  if (ajaxOptions.data && typeof ajaxOptions.data === 'object') {
    var formData = new FormData();
    Object.keys(ajaxOptions.data).forEach(function (key) {
      var formValue = ajaxOptions.data[key];      
      if (Array.isArray(formValue)) {
        formValue.forEach(function (val, index) {
          formData = appendFormData(formData, `${key}[${index}]`, val);
        })
      } else {
        formData = appendFormData(formData, key, formValue);
      }
    });
    ajaxOptions.data = formData;
    ajaxOptions.headers['X-Atlassian-Token'] = 'no-check';
  } else {
    throw new Error('For a Multipart request, data must to be an Object');
  }
  return ajaxOptions;
}

export default {
  /**
  * Execute an XMLHttpRequest in the context of the host application. The format of the response (dataType) will always be set to "text" - even if specified.
  *
  * @param {String}         url                           Either the URI to request or an options object (as below) containing at least a 'url' property;<br />
  *                                                       This value should be relative to the context path of the host application.
  * @param {Object}         options                       The options of the request.
  * @param {String}         options.url                   The url to request from the host application, relative to the host's context path
  * @param {String}         [options.type=GET]            The HTTP method name.
  * @param {Boolean}        [options.cache=true]          If the request should be cached.
  * @param {String|Object}  [options.data]                The body of the request; required if type is 'POST' or 'PUT'
  * @param {String}         [options.contentType]         The content-type string value of the entity body, above; required when data is supplied.
  * @param {Object}         [options.headers]             An object containing headers to set; supported headers are: 'Accept', 'If-Match' and 'If-None-Match'.
  * @param {Function}       [options.success]             A callback function executed on a 200 success status code.
  * @param {Function}       [options.error]               A callback function executed when a HTTP status error code is returned.
  * @param {Boolean}        [options.experimental=false]  If this is set to true, the developer acknowledges that the API endpoint which is being called may be in beta state, and thus may also have a shorter deprecation cycle than stable APIs.
  * @example
  * // A simple POST request which logs response in the console.
  * AP.request({
  *   url: '../assets/js/rest-example.json',
  *   type: 'POST',
  *   data: {name: 'some text', description: 'test'}
  *   success: function(responseText){
  *     console.log(responseText);
  *   },
  *   error: function(xhr, statusText, errorThrown){
  *     console.log(arguments);
  *   }
  * });

  * @example
  * // Upload an attachment to a Confluence entity.
  * var fileToUpload = document.getElementById("fileInput").files[0];
  *
  * AP.request({
  *   url: '/rest/api/content/123456/child/attachment',
  *   type: 'POST',
  *   contentType: 'multipart/form-data',
  *   data: {comment: 'example comment', file: fileToUpload},
  *   success: function(responseText){
  *     alert(responseText);
  *   }
  * });
  */
  request: function(args, callback){
    var url = AJS.contextPath();
    var headers = {};

    callback = arguments[arguments.length - 1];
    if(typeof args === 'string') {
      args = { url: args};
    }

    url += args.url;
    url = url.replace(/\/\.\.\//ig,''); // strip /../ from urls

    $.each(args.headers || {}, function (k, v) {
      headers[k.toLowerCase()] = v;
    });

    function done(data, textStatus, xhr) {
      callback(false, toJSON(xhr), data);
    }
    function fail(xhr, textStatus, errorThrown) {
      callback(errorThrown, toJSON(xhr), errorThrown);
    }

    // Disable system ajax settings. This stops confluence mobile from injecting callbacks and then throwing exceptions.
    // $.ajaxSettings = {};

    // execute the request with our restricted set of inputs
    var ajaxOptions = {
      url: url,
      type: args.type || 'GET',
      data: args.data,
      dataType: 'text', // prevent jquery from parsing the response body
      contentType: args.contentType,
      cache: (typeof args.cache !== 'undefined') ? !!args.cache : undefined,
      headers: {
        // */* will undo the effect on the accept header of having set dataType to 'text'
        'Accept': headers.accept || '*/*',
        // send the client key header to force scope checks
        'AP-Client-Key': callback._context.extension.addon_key
      }
    };

    // if it's a multipart request, then transform data into a FormData object
    if (ajaxOptions.contentType === 'multipart/form-data') {
      ajaxOptions = handleMultipartRequest(ajaxOptions);
    }

    // set the experimental header
    if (args.experimental === true) {
      ajaxOptions.headers["X-ExperimentalApi"] = "opt-in";
    }

    $.each(REQUEST_HEADERS_WHITELIST, function (index, header) {
      if (headers[header.toLowerCase()]) {
        ajaxOptions.headers[header] = headers[header.toLowerCase()];
      }
    });

    $.ajax(ajaxOptions).then(done, fail);
  }
};
